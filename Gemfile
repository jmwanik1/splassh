source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use redis for action cable
gem 'redis', '3.3.1'
# Use sidekiq for active jobs
gem 'sidekiq'
# Use haml for views
gem 'haml'
# Use devise for authentication
gem 'devise'
# Use bootstrap for styling
gem 'bootstrap-sass', "~> 3.3"
# Use font awesome for icons
gem "font-awesome-rails", "~> 4.3"
# User simple forms as forms
gem 'simple_form'
#  gmaps4rails for displaying maps
gem 'gmaps4rails'
# Ancestry for nested trees
gem 'ancestry'
# Use pundit for roles and authorizations
gem 'pundit', '~> 0.3.0'
# Use  kaminari for pagination
gem 'kaminari'
# Use for converting utc to localtime zone
gem 'local_time'
# Use friendly_id for showing pretty url 
gem 'friendly_id'
# Use carrierWave for file uploads
gem 'carrierwave'
# Use mini_magic for image resizing
gem 'mini_magick'
# Use fog for production files
gem 'fog'
# Use rest-client for
gem 'rest-client'
# Use underscore js
gem 'underscore-rails'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'record_tag_helper', '~> 1.0'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'momentjs-rails'

gem 'bootstrap-datepicker-rails'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'rspec-rails'
  gem 'capybara'
  gem 'factory_girl_rails'
  gem 'faraday'
end
gem 'rails_12factor', group: [:staging, :production]
group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# To implement the slider on the front page
gem 'owlcarousel-rails'

# To share certain items to social media
gem 'social-share-button'
# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
