Rails.application.routes.draw do
  resources :activities
  root "pages#home"
  
  # pages 
  get 'home',     to: 'pages#home',     as: :home
  get 'about',    to: 'pages#about',    as: :about
  get 'team',     to: 'pages#team',     as: :team
  get 'contact',  to: 'pages#contact',  as: :contact
  get 'faq',      to: 'pages#faq',      as: :faq
  get 'donate',   to: 'pages#donate',   as: :donate
  get 'sponsors', to: 'pages#sponsors', as: :sponsors
  get 'terms',    to: 'pages#terms',    as: :terms
  get 'privacy',  to: 'pages#privacy',  as: :privacy
  get 'testimonials',    to: 'pages#testimonials',    as: :testimonials
  get 'metrics',  to: 'pages#metrics',  as: :metrics
  
  # ph datasets get request
  get 'update_selection', to: 'ph_datasets#update_selection', as: :update_selection
  get 'retrieve_data', to: 'ph_datasets#retrieve_data', as: :retrieve_data
  get 'ph',  to: 'ph_datasets#index',  as: :ph
  get 'pH',  to: 'ph_datasets#index',  as: :pH
  get 'portal',  to: 'ph_datasets#index',  as: :portal
  
  match 'admin/users/:id' => 'admin/users#destroy', :via => :delete, 
                      :as => :destroy_admin_user
  match 'admin/projects/:id' => 'admin/projects#destroy', :via => :delete, 
                      :as => :destroy_admin_project
                      
  resources :projects do
    resources :scientific_data
    resources :comments 
    resources :contributors, only: [:index, :create, :destroy]
    resources :field_notes
    resources :sites
  end
  
  resources :debris do 
    resources :comments 
  end
  
  devise_for :users, :controllers => { registrations: 'registrations' }
  resources :users do
    resources :profiles   
  end
  
  resources :explore, only: [:index]
  
  resources :comments do 
    resources :comments
  end
  
  namespace :admin do
    root 'application#index'
    resources :users do 
      member do
        patch :archive
      end
    end
    
    resources :projects do 
      member do
        patch :archive
      end
    end
    
    resources :states do
      member do
        get :make_default
      end
    end
    
    resources :secoora_datasets do
      collection do
        get :process_data
      end
    end
    resources :teams
    resources :body_of_waters
    resources :tags
  end
  
  resources :trackers do 
    resources :items
    resources :comments
    member do 
        get 'manage'
    end
  end
  
  resources :items, only: [] do
    resources :statuses, only: [:create]
  end
  
  resources :notifications do
    collection do 
      post :mark_as_read
    end
  end
  
  resources :tags, only: [:index, :show]
  
  resources :favorite_projects, only: [:create, :destroy]
  
  resources :ph_datasets do
    collection do
      get :update_data
      get 'clean_data', to: 'ph_datasets#clean_data', as: :clean_data
    end
  end
  
  mount ActionCable.server, at: '/cable'
end
