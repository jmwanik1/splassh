require 'rails_helper'

RSpec.feature "Users can edit existing projects" do
  let(:author) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: author.id)
  end
  
  before do
    login_as(author)
  end
  
  scenario "with valid attributes" do
    FactoryGirl.create(:project, name: "Sample water project", author: author,
                lat: 34.1, lng: -19.6)
    
    visit "/projects"
    click_link "Edit"
  
    fill_in "Project name", with: "Edited sample water project"
    click_button "Update Project"
    
    expect(page).to have_content "Project has been updated."
    expect(page).to have_content "Edited sample water project"
    
  end
end