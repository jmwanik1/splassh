require "rails_helper"

RSpec.feature "Users can view projects" do 
  let!(:user) { FactoryGirl.create(:user, :admin) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  let!(:project) { FactoryGirl.create(:project, name: "An example project",
                                      author_id: user.id)}
  
  
  before do
    login_as(user)
    assign_role!(user, :viewer, project)
    visit "/projects"
  end
  
  scenario "with the project details" do
    expect(page).to have_content "An example project"
    first(:link, "An example project").click
    expect(page.current_url).to eq project_url(project)
  end
end


