require 'rails_helper'

RSpec.feature "Users can create a project" do 
  let!(:user) { FactoryGirl.create(:user, :admin) }
  let!(:profile) do
    FactoryGirl.create(:profile, user: user)
  end
  
  let!(:bow) { FactoryGirl.create(:body_of_water, name: "Rivers") }
  let!(:tag) { FactoryGirl.create(:tag, name: "Test Tag") }
  
  before do
    login_as(user)
    visit "/projects"
    click_link "New Project"  
  end
  
  scenario "with valid attributes" do
    fill_in "Project name", with: "Splassh project"
    fill_in "Body of Water Name", with: "Splassh project body of water"
    fill_in "project_description", with: "Deserve a good framework"
    fill_in "project_lat", with: 34.1
    fill_in "project_lng", with: -19.5
    select "Rivers", :from => "Body of Water"
    select "Test Tag", :from => "Tag"
    attach_file "Project Cover Photo", "spec/fixtures/cover.jpg"
    attach_file "File #1", "spec/fixtures/sample.1.txt"
    
    click_button "Create Project"
    
    expect(page).to have_content "Project has been created."
  end
  
  scenario  "with invalid attributes" do
    click_button "Create Project"
    
    expect(page).to have_content "The form contains"
  end
end