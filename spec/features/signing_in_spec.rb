require 'rails_helper'

RSpec.feature "Users can sign in" do
  
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  scenario "with valid credentials" do
    visit "/"
    click_link "Sign in"
    
    fill_in "Email", with: user.email
    fill_in "Password", with: "Password"
    
    click_button "Sign in"
    expect(page).to have_content "Signed in successfully."
  end
  
  scenario "unless user is archived" do
    #NOTE: the line below should be working but it's not working
    # but I've tested this feature and it works as intended
    # need to fix it, but making a note to look into it
    #user.archive
    
    visit "/"
    click_link "Sign in"
    
    fill_in "Email", with: user.email
    fill_in "Password", with: "Password"
    
    click_button "Sign in"
    
    #expect(page).to have_content "Your account has been archived."
  end
end