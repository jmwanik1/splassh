require 'rails_helper'

RSpec.feature "Admin can delete users" do
  
  let(:admin_user) { FactoryGirl.create(:user, :admin) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: admin_user.id)
  end
  let(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  before do
    login_as(admin_user)
    visit admin_user_path(user)
  end
  
  scenario "with valid attributes" do 
    first(:link, "Delete").click
    expect(page).to have_content "User has been deleted."
  end
end