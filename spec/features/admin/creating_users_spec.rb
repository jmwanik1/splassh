require "rails_helper"

RSpec.feature "Admins can create new users" do
  let(:admin) { FactoryGirl.create(:user, :admin) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: admin.id)
  end
  
  before do
    login_as(admin)
    visit "/"
    click_link "Admin"
    click_link "Users"
    click_link "New User"
  end
  
  scenario "with valid credentials" do 
    fill_in "Email", with: "example@email.com"
    fill_in "user_password", with: "password"
    fill_in "Password confirmation", with: "password"
    click_button "Create User"
    expect(page).to have_content("User has been created.")
    expect(page).to have_content("example@email.com")
  end
  
  scenario "when the nuew user is an admin" do
    fill_in "Email", with: "admin@example.com"
    fill_in "user_password", with: "password"
    fill_in "Password confirmation", with: "password"
    check "Is an admin?"
    click_button "Create User"
    expect(page).to have_content("User has been created.")
    expect(page).to have_content("admin@example.com")
  end
end