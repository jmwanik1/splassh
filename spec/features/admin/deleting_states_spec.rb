require 'rails_helper'

RSpec.feature "Admins can edit states" do
  let!(:state) { FactoryGirl.create(:state, name: "Default") }
  
  before do 
    login_as(FactoryGirl.create(:user, :admin))
    visit admin_states_path
  end
  
  scenario "With valid attributes" do
    first(:link, "Delete").click
    
    expect(page).to have_content "State has been deleted."
    expect(page).not_to have_content "Done"
  end
end