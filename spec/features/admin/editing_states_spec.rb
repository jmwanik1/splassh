require 'rails_helper'

RSpec.feature "Admins can edit states" do
  let!(:state) { FactoryGirl.create(:state, name: "Default") }
  
  before do 
    login_as(FactoryGirl.create(:user, :admin))
    visit admin_states_path
  end
  
  scenario "With valid attributes" do
    first(:link, "Edit").click
    
    expect(page).to have_content "Default"
    
    fill_in "Name", with: "Done"
    fill_in "Color", with: "Blue"
    click_button "Update State"
    
    expect(page).to have_content "State has been updated."
    expect(page).to have_content "Done"
  end
end