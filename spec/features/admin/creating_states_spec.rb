require 'rails_helper'

RSpec.feature "Admins can create new states for items" do
  
  before do 
    login_as(FactoryGirl.create(:user, :admin))
  end
  
  scenario "with valid details" do
    visit admin_root_path
    click_link "States"
    click_link "New State"
    
    fill_in "Name", with: "Done"
    fill_in "Color", with: "Green"
    click_button "Create State"
    
    expect(page).to have_content "State has been created."
  end
end