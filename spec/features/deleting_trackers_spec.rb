require 'rails_helper'

RSpec.feature "Users can delete a trackers item" do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  let!(:tracker) do
    FactoryGirl.create(:tracker, name: "An example tracker",
                                      user_id: user.id)
  end
  
  before do
    login_as(user)
  end
    
  scenario "successfully" do
    visit "/trackers"
    
    page.first(:link, "Delete").click
    
    expect(page).to have_content "Tracker has been deleted."
    expect(page).to_not have_content "An example tracker"
  end
end