require "rails_helper"

RSpec.feature "Users can edit a tracker" do 
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  let!(:tracker) do
    FactoryGirl.create(:tracker, name: "An example tracker",
                                      user_id: user.id)
  end
  
  before do
    login_as(user)
    visit "/trackers"
  end
  
  scenario "with valid attributes" do
    first(:link, "Edit").click
    
    expect(page).to have_content "An example tracker"
    
    fill_in "Tracker name", with: "Tracks useful stuff"
    click_button "Update Tracker"
    
    expect(page).to have_content "Tracker has been updated."
    expect(page).to have_content "Tracks useful stuff"
  end
end