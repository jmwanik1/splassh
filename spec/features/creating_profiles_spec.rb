require "rails_helper"

RSpec.feature "Users can create profiles" do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  before do
    visit user_profiles_path(user)
    click_link "New Profile"
  end
  
  scenario "successfully" do
    fill_in "First Name", with: "John"
    fill_in "Last Name", with: "Smith"
    fill_in "Occupation", with: "Dev"
    fill_in "About", with: "Sample about"
    fill_in "School", with: "LSU"
    fill_in "Education Level", with: "Some Level"
    
    click_button "Create Profile"
    expect(page).to have_content "Profile has been created."
  end
end