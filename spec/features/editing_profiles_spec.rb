require "rails_helper"

RSpec.feature "Users can create profiles" do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  before do
    visit user_profiles_path(user)
    click_link "Edit Profile"
  end
  
  scenario "successfully" do
    fill_in "First Name", with: "James"
    fill_in "Last Name", with: "Smith"
  
    click_button "Update Profile"
    expect(page).to have_content "Profile has been updated."
  end
end