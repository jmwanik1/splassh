require "rails_helper"

RSpec.feature "Users can create a new item to track" do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  let!(:state) { FactoryGirl.create(:state, name: "DefaultState", default: true) }
  
  before do
    login_as(user)
    tracker = FactoryGirl.create(:tracker, name: "An example tracker", user_id: user.id)
    
    visit tracker_path(tracker)
    click_link "New Item"  
  end
  
  scenario "with valid attributes" do
    fill_in "Item name", with: "An item to be tracked"
    fill_in "description", with: "Description of item"
    attach_file "File #1", "spec/fixtures/sample.1.txt"
    click_button "Create Item"
    
    expect(page).to have_content "Item has been created."
    expect(page).to have_content "DefaultState"
    expect(page).to have_content "sample.1.txt"
  end
  
  scenario "with invalid attributes" do
    click_button "Create Item"
    
    expect(page).to have_content "Item has not been created."
    expect(page).to have_content "Name can't be blank"
    expect(page).to have_content "Description can't be blank"
  end
  
  scenario "persisting file uploads accross form displays" do
    attach_file "File #1", "spec/fixtures/sample.1.txt"
    attach_file "File #2", "spec/fixtures/sample.2.txt"
    attach_file "File #3", "spec/fixtures/sample.3.txt"
    
    click_button "Create Item"
    
    fill_in "Item name", with: "An item to be tracked"
    fill_in "description", with: "Description of item"
    click_button "Create Item"
    
    expect(page).to have_content "sample.1.txt"
    expect(page).to have_content "sample.2.txt"
    expect(page).to have_content "sample.3.txt"
  end
end