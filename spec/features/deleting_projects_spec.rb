require 'rails_helper'

RSpec.feature "Users can delete a project" do
  let(:author) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: author.id)
  end
  
  before do
    login_as(author)
  end
  
  scenario "successfully" do
    FactoryGirl.create(:project, name: "Sample water project", author: author,
                lat: 34.1, lng: -19.1)
    
    visit "/projects"
    page.click_link("Delete")
    
    expect(page).to have_content "Project has been deleted."
    expect(page.current_url).to eq projects_url
    expect(page).to have_no_content "Sample water project"
  end
end