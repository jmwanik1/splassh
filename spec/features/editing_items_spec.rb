require "rails_helper"

RSpec.feature "Users can view items" do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  let!(:tracker) do
    FactoryGirl.create(:tracker, name: "An example tracker",
                                      user_id: user.id)
  end
  
  before do
    login_as(user)
    FactoryGirl.create(:item, tracker: tracker, name: "Cans",
                      description: "Some item description")
    visit "/trackers"
  end
  
  scenario "with valid attributes" do
    first(:link, "An example tracker").click
    
    first(:link, "Edit").click
    
    expect(page).to have_content "Cans"
    expect(page).to have_content "Some item description"
    
    fill_in "Item name", with: "Soda cans"
    click_button "Update Item"
    
    expect(page).to have_content "Item has been updated."
    expect(page).to have_content "Soda cans"
  end
end