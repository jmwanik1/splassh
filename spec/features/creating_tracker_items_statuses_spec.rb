require "rails_helper"

RSpec.feature "Users can create a status on tracker items" do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  let!(:tracker) do
    FactoryGirl.create(:tracker, name: "An example tracker",
                                      user_id: user.id)
  end
  
  let(:item) do 
      FactoryGirl.create(:item, tracker: tracker, name: "Cans",
                      description: "Some item description")
    end
  
  let!(:state) do
      FactoryGirl.create(:state, name: "Open")
    end
  
  before do
    login_as(user)
    visit tracker_item_path(tracker, item)
  end
  
  scenario"with valid attributes" do
    fill_in "Status", with: "Open"
    click_button "Create Status"
    
    expect(page).to have_content "Status has been created."
  end
  
  scenario "with invalid attributes" do
    click_button "Create Status"
    expect(page).to have_content "Status has not been created."
  end
  
  scenario "when changing an item's state" do
    fill_in "Status", with: "Open"
    select "Open", from: "status_state_id"
    click_button "Create Status"
    expect(page).to have_content "Status has been created."
    expect(page).to have_content "Open"
    within("#statuses") do
      expect(page).to have_content "state changed to Open"
    end
  end
end