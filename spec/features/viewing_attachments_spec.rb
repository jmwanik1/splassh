require 'rails_helper'

RSpec.feature "Users can view an item's attached files" do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  let!(:tracker) do
    FactoryGirl.create(:tracker, name: "An example tracker",
                                      user_id: user.id)
  end
  let!(:item) do
    FactoryGirl.create(:item, tracker: tracker, 
    file_to_attach: "spec/fixtures/sample.1.txt")
  end
end