require "rails_helper"

RSpec.feature "Users can view items" do
  let!(:user) { FactoryGirl.create(:user, :admin) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  let!(:tracker) do
    FactoryGirl.create(:tracker, name: "An example tracker",
                                      user_id: user.id)
  end
  
  before do
    login_as(user)
    FactoryGirl.create(:item, tracker: tracker, name: "Cans",
                      description: "Some item description")
    visit "/trackers"
  end
  
  scenario "view an item for a given tracker" do
    expect(page).to have_content "An example tracker"
    first(:link, "An example tracker").click
    
    expect(page).to have_content "Cans"
    first(:link, "Cans").click
    
    #expect(page).to have_content "Some item description"
  end
end