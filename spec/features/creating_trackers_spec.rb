require "rails_helper"

RSpec.feature "Users can create a tracker" do
  let!(:user) { FactoryGirl.create(:user, :admin) }
  let!(:profile) do
    FactoryGirl.create(:profile, user_id: user.id)
  end
  
  before do
    login_as(user)
    visit "/trackers"
    click_link "New Tracker"  
  end
  
  scenario "with valid attributes" do
    fill_in "Tracker name", with: "Debris tracker"
    fill_in "Tracker description", with: "Tracks debris in an example river"
    fill_in "tracker_lat", with: 34.1
    fill_in "tracker_lng", with: -19.5
    
    click_button "Create Tracker"
    
    expect(page).to have_content "Tracker has been created."
  end
  
  scenario  "with invalid attributes" do
    click_button "Create Tracker"
    
    expect(page).to have_content "The form contains"
  end
  
end