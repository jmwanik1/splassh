require 'rails_helper'

RSpec.feature "Routes" do
  scenario "to home" do
    visit "/home"
    expect(current_path).to eq home_path
  end
  scenario "to about" do
    visit "/about"
    expect(current_path).to eq about_path
  end
  scenario "to team" do
    visit "/team"
    expect(current_path).to eq team_path
  end
  scenario "to faq" do
    visit "/faq"
    expect(current_path).to eq faq_path
  end
  scenario "to donate" do
    visit "/donate"
    expect(current_path).to eq donate_path
  end
  scenario "to sponsors" do
    visit "/sponsors"
    expect(current_path).to eq sponsors_path
  end
  scenario "to terms" do
    visit "/terms"
    expect(current_path).to eq terms_path
  end
  scenario "to privacy" do
    visit "/privacy"
    expect(current_path).to eq privacy_path
  end
end 