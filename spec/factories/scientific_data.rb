FactoryGirl.define do
  factory :scientific_datum do
    category "MyString"
    name "MyString"
    value "9.99"
    unit "MyString"
    instrument "MyString"
    technique "MyString"
    calibration "MyString"
    info "MyText"
  end
end
