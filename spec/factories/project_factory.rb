FactoryGirl.define do
  factory :project do   
    name "Example water project"
    description "Example water project description"
    water_body "Example water body"
    lat 34.1
    lng -19.5
  end
end