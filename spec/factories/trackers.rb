FactoryGirl.define do
  factory :tracker do
    name "Example tracking project"
    description "Tracks stuff"
    lat 34.1
    lng -14.7
  end
end
