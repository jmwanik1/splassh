FactoryGirl.define do
  factory :activity do
    user nil
    trackable_type "MyString"
    trackable_id 1
    action "MyString"
  end
end
