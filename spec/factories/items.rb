FactoryGirl.define do
  factory :item do
    name "An example item"
    description "An example item description"
  end
end
