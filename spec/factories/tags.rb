FactoryGirl.define do
  factory :tag do
    name "MyString"
    color "MyString"
    description "MyText"
  end
end
