FactoryGirl.define do
  factory :status do
    content {"A status describing what happened to an item" }
    item nil
    author nil
  end
end
