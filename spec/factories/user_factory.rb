FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "test#{n}@email.com" }
    password "Password"
    
    trait :admin do
      admin true
    end
  end
end