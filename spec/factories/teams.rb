FactoryGirl.define do
  factory :team do
    member "MyString"
    about "MyText"
    facebook "MyString"
    linkedin "MyString"
    twitter "MyString"
  end
end
