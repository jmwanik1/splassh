FactoryGirl.define do
  factory :notification do
    by_id 1
    to_id 1
    read_at "2017-05-21 16:02:33"
    action "MyString"
    notifiable_id 1
    notifiable_type "MyString"
  end
end
