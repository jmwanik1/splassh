FactoryGirl.define do
  factory :ph_dataset do
    dataset_id "MyString"
    title "MyString"
    institution "MyString"
    url "MyString"
    time_series_variables "MyString"
    time_coverage_start "MyString"
    time_coverage_end "MyString"
    lat ""
    lng ""
  end
end
