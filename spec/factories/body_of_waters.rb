FactoryGirl.define do
  factory :body_of_water do
    name "MyString"
    desc "MyText"
    color "MyString"
  end
end
