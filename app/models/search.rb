class Search
  attr_reader :where_clause, :where_args, :order
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  
  def initialize(search_term)
    search_term = search_term.downcase
    @where_clause = ""
    @where_args = {}
    
    # search for user if provided an email
    if search_term =~ VALID_EMAIL_REGEX  
      build_for_email_search(search_term)
    else
      build_for_name_search(search_term)
    end
  end
  
  def build_for_name_search(search_term)
    @where_clause << case_insensitive_search(:name)
    @where_args[:name] = starts_with(search_term)
    
    @order = "name asc"
  end
  
  def build_for_email_search(search_term)
    @where_clause << case_insensitive_search(:email)
    @where_args[:email] = starts_with(search_term)
    
    @order = "email asc"
  end
  
  
  
  def starts_with(search_term)
    "%" + search_term + "%"
  end
  
  def case_insensitive_search(field_name)
    "lower(#{field_name}) like :#{field_name}"
  end
end
