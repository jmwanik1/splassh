class Status < ApplicationRecord
  # Associations
  belongs_to :item
  belongs_to :author, class_name: "User"
  belongs_to :state
  belongs_to :previous_state, class_name: "State"
  
  # Scopes
  scope :persisted, lambda { where.not(id: nil) }
  scope :desc,      lambda { order('created_at DESC')  }
  
  #Validations
  # Uncomment the line below to require users to enter
  # a description when submiting state/status
  # validates :content, presence: false
  
  
  # Callbacks
  before_create :set_previous_state
  before_save :check_state_uniqueness
  after_create :set_item_state
  
  private
    def set_item_state
      item.state = state
      item.save!
    end
    
    def set_previous_state
      self.previous_state = item.state
    end
    
    def check_state_uniqueness
      if self.state == item.state
        errors.add(:base, "State cannot be the same, it must change.")
        throw(:abort)
      else
        true
      end
    end
end
