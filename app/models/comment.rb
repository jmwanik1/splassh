class Comment < ApplicationRecord
  # Associations
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  has_many :comments, dependent: :destroy, as: :commentable
  has_many :users, through: :comments , source: 'user'
  has_many :activities, dependent: :destroy, as: :trackable
  has_many :notifications, dependent: :destroy, as: :notifiable
  
  # Validations
  validates :content, presence: true
  
  # Jobs
  # after_create_commit { RenderCommentJob.perform_later self }
  
  #Scopes
  scope :desc, lambda { order('created_at DESC') }
  scope :persisted, lambda { where.not(id: nil) }
end
