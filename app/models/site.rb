class Site < ApplicationRecord
    # Required associations  
  belongs_to :project
  belongs_to :user
  has_many :activities, dependent: :destroy, as: :trackable
  
  # Validations 
  validates :name, :description, :lat, :lng, :user,
            presence: true
end

