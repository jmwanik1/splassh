class Favorite < ApplicationRecord
  belongs_to :user
  belongs_to :favorited, polymorphic: true
  has_many :activities, dependent: :destroy, as: :trackable
  
  scope :ord_recent, lambda { order('created_at DESC') }
  scope :ord_oldest, lambda { order('created_at ASC') }
end

