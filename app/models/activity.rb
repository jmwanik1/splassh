class Activity < ApplicationRecord
  belongs_to :user
  belongs_to :trackable, polymorphic: true
  scope :desc,  lambda { order('created_at DESC')  }
  scope :asc,      lambda { order('created_at ASC')  }
end
