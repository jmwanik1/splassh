class Tracker < ApplicationRecord
  # Associations
  belongs_to :user
  belongs_to :project
  has_many :items, dependent: :destroy
  has_many :activities, dependent: :destroy, as: :trackable
  has_many :comments, dependent: :destroy, as: :commentable 
  has_many :users, through: :comments, source: 'user'

  # Scopes
  scope :ord_recent, lambda { order('created_at DESC') }
  scope :ord_oldest, lambda { order('created_at ASC') }
  
  # Validations
  validates :name, :description, :lat, :lng, presence: true
  validates :description, length: { maximum: 1000 }
  
  # Friendly url 
  include FriendlyId
  friendly_id :name, :use => [:history, :slugged, :finders]
  
  def should_generate_new_friendly_id?
    name_changed?
  end
end
