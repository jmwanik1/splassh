class Debri < ApplicationRecord
  validates :title, :description, :lat, :lng, presence: true
  has_many :comments, dependent: :delete_all, as: :commentable 
end
