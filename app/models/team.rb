class Team < ApplicationRecord
  validates :name, presence: true
  mount_uploader :image, SplasshMembersPhotoUploader

  scope :recent, lambda { order('created_at DESC') }
  scope :oldest, lambda { order('created_at ASC') }
  
  scope :current, lambda { where(left: false) }
  scope :past, lambda { where(left: true) }
end
