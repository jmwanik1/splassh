class Item < ApplicationRecord
  # Associations
  belongs_to :tracker
  belongs_to :state
  belongs_to :user
  has_many :attachments, dependent: :destroy
  has_many :statuses, dependent: :destroy
  has_many :activities, dependent: :destroy, as: :trackable
  
  accepts_nested_attributes_for :attachments, reject_if: :all_blank
  
  # Validations
  validates :name, :lat, :lng, presence: true
  validates :description, length: { maximum: 1000 }
  
  # scopes
  scope :persisted, lambda { where.not(id: nil) }
  scope :desc, lambda { order('created_at DESC') }
  scope :asc, lambda { order('created_at ASC') }
  
  # Callbacks 
  before_create :assign_default_state
  
  max_paginates_per 5
  
  def color
    state.color
  end
  
  private
    def assign_default_state
      self.state ||= State.default
    end
end
