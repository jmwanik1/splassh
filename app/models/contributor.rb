class Contributor < ApplicationRecord
  belongs_to :user
  belongs_to :contributable, polymorphic: true
  has_many :activities, dependent: :destroy, as: :trackable

  validates_uniqueness_of :user, :scope => :contributable
end
