class Notification < ApplicationRecord
  belongs_to :by, class_name: "User"
  belongs_to :to, class_name: "User"
  belongs_to :notifiable, polymorphic: true
  scope :unread, lambda { where(read_at: nil) }
  scope :ord_recent, lambda { order('created_at DESC') }
  scope :ord_oldest, lambda { order('created_at ASC') }
end
