class Project < ApplicationRecord
  # Associations
  belongs_to :author, class_name: 'User', foreign_key: 'author_id'
  belongs_to :body_of_water
  belongs_to :tag
  has_many :scientific_data, dependent: :destroy
  has_many :site, dependent: :destroy
  has_many :comments, dependent: :destroy, as: :commentable 
  has_many :users, through: :comments, source: 'user'
  has_many :impressions, dependent: :destroy, as: :impressionable
  has_many :activities, dependent: :destroy, as: :trackable
  has_many :attachments, dependent: :destroy
  has_many :notifications, dependent: :destroy, through: :comments, as: :notifiable
  has_many :favorites, dependent: :destroy, as: :favorited
  has_many :contributors, dependent: :destroy, as: :contributable
  has_many :field_notes, dependent: :destroy
  has_many :trackers, dependent: :destroy
  # Uploaders
  mount_uploader :cover_photo, CoverPhotoUploader
  
  # Attachments
  accepts_nested_attributes_for :attachments, allow_destroy: true
  
  # Scopes
  scope :ord_popular, lambda { order('impressions_count DESC') }
  scope :ord_recent, lambda { order('created_at DESC') }
  scope :ord_oldest, lambda { order('created_at ASC') }
  # scope :excluding_archived, lambda { where(archived_at: nil)}
  
  # Nested attributes
  accepts_nested_attributes_for :attachments, reject_if: :all_blank

  # Validations 
  validates :name, :description, :lat, :lng, 
            :body_of_water_id, :tag_id, :cover_photo, presence: true
  
  # Friendly url 
  include FriendlyId
  friendly_id :name, :use => [:history, :slugged, :finders]
  
  # Max number of projects per page 
  max_paginates_per 10
  
  def should_generate_new_friendly_id?
    name_changed?
  end
  
  def impressed(user_id, ip_address, impressionable)
    impressions.where(user_id: user_id, ip_address: ip_address, 
                      impressionable: impressionable)
  end
  
  def contributor(user_id)
    User.find(user_id)
  end
end
