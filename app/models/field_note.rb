class FieldNote < ApplicationRecord
  belongs_to :project
  belongs_to :user
  
  validates :note, presence: true
  
  scope :persisted, lambda { where.not(id: nil) }
  scope :ord_recent, lambda { order('created_at DESC') }
  scope :ord_oldest, lambda { order('created_at ASC') }
end