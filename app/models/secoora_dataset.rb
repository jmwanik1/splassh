class SecooraDataset < ApplicationRecord
  validates :dataset_url, :format => URI::regexp(%w(http https)),
            presence: true
end
