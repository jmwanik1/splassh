class ScientificDatum < ApplicationRecord
  # Required associations  
  belongs_to :project
  belongs_to :category
  has_many :activities, dependent: :destroy, as: :trackable
  belongs_to :user
  
  # Optional associations 
  belongs_to :chemical, optional: true
  belongs_to :biological, optional: true
  belongs_to :physical, optional: true
  
  # Validations 
  validates :unit, :instrument, :technique, :calibration,
            presence: true
end
