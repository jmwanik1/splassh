class User < ApplicationRecord
  # Associations 
  has_one  :profile, dependent: :destroy
  has_many :projects, dependent: :destroy, foreign_key: 'author_id'
  has_many :comments, dependent: :destroy
  has_many :notifications, dependent: :destroy, foreign_key: :to_id
  has_many :activities, dependent: :destroy
  has_many :trackers, dependent: :destroy
  has_many :items, dependent: :destroy, through: :trackers
  has_many :favorites
  has_many :favorite_projects, through: :favorites, 
            source: :favorited, source_type: 'Project'
  has_many :contributors
  has_many :contributor_projects, through: :contributors, 
            source: :contributable, source_type: 'Project'
  accepts_nested_attributes_for :profile
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
        :recoverable, :rememberable, :trackable, :validatable
        
  # Scopes
  scope :excluding_archived, lambda { where(archived_at: nil)}
  
  # Max number of users per page 
  max_paginates_per 15
  
  def to_s
    "#{email} (#{admin? ? "Admin" : "User"})"
  end
  
  def username
    # "#{profile.firstname} #{profile.lastname}" 
  end
  
  def active_for_authentication?
    super && archived_at.nil?
  end
  
  def inactive_message
    archived_at.nil? ? super : :archived
  end
  
  protected
    def confirmation_required?
      false
    end
end
