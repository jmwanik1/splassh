class State < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  #Scopes
  scope :desc, lambda { order('created_at DESC')  }
  scope :asc,  lambda { order('created_at ASC')  }
  scope :excluding_unavailable, lambda { where(available: true)}
  
  # Max number of states per page 
  max_paginates_per 25
  
  def make_default!
    State.update_all(default: false)
    update!(default: true)
  end
  
  def self.default
    find_by(default: true)
  end
  
  def to_s
    name
  end
end
