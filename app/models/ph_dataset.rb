class PhDataset < ApplicationRecord
  validates :dataset_id, :presence => true, 
          :uniqueness => { :case_sensitive => false }
  scope :ord_recent, lambda { order('created_at DESC') }
  scope :ord_oldest, lambda { order('created_at ASC') }
  max_paginates_per 15
end
