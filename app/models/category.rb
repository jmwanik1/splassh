class Category < ApplicationRecord
  has_many :scientific_data, dependent: :delete_all
  validates :name, presence: true
end
