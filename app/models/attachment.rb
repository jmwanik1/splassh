class Attachment < ApplicationRecord
  belongs_to :item
  belongs_to :project
  mount_uploader :file, AttachmentUploader
end
