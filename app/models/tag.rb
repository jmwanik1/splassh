class Tag < ApplicationRecord
  # Associations 
  has_many :projects, dependent: :destroy
  
  # Validations
  validates :name, presence: true, uniqueness: true
end
