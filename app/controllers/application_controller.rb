class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  # Log a visit to a unique page 
  # In this case, a unique visit means a unique ip and user or unique ip and 
  # non signed in user
  def log_impression
    id = if user_signed_in? then current_user.id else 0 end
    if not @project.impressed(id, request.remote_ip, @project).present?
      @project.impressions.create(user_id: id, 
                                  ip_address: request.remote_ip,
                                  impressionable: @project)
    end
  end
  
  def track_activity(trackable, action=params[:action])
    trackable.activities.create!(user: current_user, action: action, 
                                    trackable: trackable)
  end
  
  private
    def after_sign_in_path_for(resource)
      user_profiles_path(current_user)
    end
end
