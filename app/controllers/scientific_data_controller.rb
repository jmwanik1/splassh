class ScientificDataController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :destroyw]
  before_action :set_project, only: [:new, :create, :edit, :show, :destroy]
  before_action :set_scientific_datum, only: [:show, :edit, :destroy]
  
  def index
    @scientific_data = ScientificDatum.where(params[:project_id])
  end
  
  def new
    @scientific_datum = @project.scientific_data.build
    @categories = Category.all
  end

  def edit
  end

  def update
    @scientific_datum = ScientificDatum.find(params[:id])
    respond_to do |format|
      if @scientific_datum.update(scientific_datum_params)
        track_activity @scientific_datum
        format.html { redirect_to @scientific_datum, 
                      notice: "The data point was successfully update." }
        format.json { render :show, status: :ok, location: @scientific_datum }
        format.js
      else
        format.html { render :update,
                      notice: "The data point could not be updated." }
        format.json { render json: @scientific_datum.errors,
                      status: :unprocessable_entity  }
        format.js
      end
    end
  end

  def destroy
    @scientific_datum.destroy
    respond_to do |format|
      format.html { redirect_to [@project, @scientific_datum],
                    notice: "Data point has been deleted." }
      format.json { head :no_content }
      format.js
    end
  end

  def create
    @scientific_datum = @project.scientific_data.build(scientific_datum_params)
    @scientific_datum.project = @project
    @scientific_datum.user = current_user
    respond_to do |format|
      if @scientific_datum.save
        track_activity @scientific_datum
        format.html { redirect_to [@project, @scientific_datum], 
                      notice: "Data was created successfully." }
        format.json { render :show, status: :created, location: @scientific_datum }
        format.js
      else
        format.html { render :new, alert: "Data could not be created. " }
        format.json { render json: @scientific_datum, status: :unprocessable_entity }
        format.js
      end
    end
  end
  
  private
  def scientific_datum_params
    params.require(:scientific_datum).permit(:category_id,:name, :unit, :value, :instrument, 
                  :technique, :calibration, :info, :chemical_id, :biological_id, 
                  :physical_id, :created_at)
  end
  
  def set_scientific_datum
    @scientific_datum = @project.scientific_data.find(params[:id])
  end
  
  def set_project
    @project = Project.friendly_id.find(params[:project_id])
  end
end
