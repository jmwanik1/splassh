class Admin::TagsController < Admin::ApplicationController
  before_action :set_tag, only: [:edit, :update, :destroy]
  
  def index
    @tags = Tag.all.order(:created_at).page params[:page]
  end
  
  def new
    @tag = Tag.new
  end
  
  def create
    @tag = Tag.new(tag_params)
    
    if @tag.save
      flash[:notice] = "Tag has been created."
      redirect_to admin_tags_path
    else
      flash[:alert] = "Tag has not been created."
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @tag.update(tag_params)
      flash[:notice] = "Tag has been updated."
      redirect_to admin_tags_path
    else
      flash[:alert] = "Tag has not been updated."
      render "edit"
    end
  end
  
  def destroy
    if @tag.destroy
      flash[:notice] = "Tag has been deleted."
      redirect_to admin_tags_path
    else
      flash[:alert] = "Tag has not been deleted."
    end
  end
  
  private 
    def set_tag
      @tag = Tag.find(params[:id])
    end
    
    def tag_params
      params.require(:tag).permit(:name, :description, :color)
    end
end
