class Admin::TeamsController < Admin::ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  def index
    @members = Team.all
  end
  
  def new
    @team = Team.new
  end
  
  def create
    @team = Team.create(team_params)
    if @team.save
      flash[:notice] = "Team member has been added."
      redirect_to admin_teams_path
    else
      flash[:alert] = "Team has not been created."
      render "new"
    end
  end
  
  def edit
  end
  
  def update
    if @team.update(team_params)
      flash[:notice] = "Team member has been updated."
      redirect_to admin_teams_path
    else
      flash[:alert] = "Team member has not been updated."
      render "edit"
    end
  end
  
  def destroy
    if @team.destroy
      flash[:notice] = "Team member has been deleted."
      redirect_to admin_teams_path
    else
      flash[:alert] = "Team member has not been deleted."
    end
  end
  
  private
    def team_params
      params.require(:team).permit(:name, :about, :role, :image, :facebook, 
                                   :website, :twitter, :linkedin, :left)
    end
    def set_team
      @team = Team.find(params[:id])
    end
end
