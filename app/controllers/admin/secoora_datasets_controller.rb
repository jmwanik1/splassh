class Admin::SecooraDatasetsController < Admin::ApplicationController
  before_action :authorize_admin!
  before_action :set_secoora_dataset, only: [:edit, :update, :show, :destroy]
  include Secoora

  def index
    @secoora_datasets = SecooraDataset.all
  end
  
  def new
    @secoora_dataset = SecooraDataset.new
  end
  
  def edit
  end
  
  def create
    @secoora_dataset = SecooraDataset.create(secoora_params)
    if @secoora_dataset.save
      flash[:notice] = "Secoora Dataset has been added"
      redirect_to admin_secoora_datasets_path
    end
  end
  
  def update
    if @secoora_dataset.update(secoora_params)
      flash[:notice] = "Secoora Data has been updated"
      redirect_to admin_secoora_datasets_path
    else
      flash[:alert] = "Secoora Dataset has not been updated"
      render 'edit'
    end
  end
  
  def show
  end
  
  def process_data
    # Process the first dataset
    # process_dataset(SecooraDataset.last.dataset_url)
    # Retrive the data
    dataset = SecooraDataset.first
    url = dataset.dataset_url
    if not dataset.processed? and secoora_data(url)
      # mark dataset as processed
      mark_processed(dataset)
      flash[:notice] = "Ph Data successfully processed"
    else
      flash[:alert] = "Ph Data were not successfully processed"
    end
    redirect_to ph_datasets_path
  end
  
  # updates the given dataset as processed
  def mark_processed(dataset)
    dataset.update(processed: true)
  end
  
  private
    def secoora_params
      params.require(:secoora_dataset).permit(:dataset_url, :about, :processed,
                                              :cleaned)
    end
    
    def set_secoora_dataset
      @secoora_dataset = SecooraDataset.find(params[:id])
    end
end