class Admin::ApplicationController < ApplicationController
  before_action :authorize_admin!
  
  def index
    # init stats
    stats
  end
  
  def stats
    # Stats for admin
    @project_count = Project.all.count
    @debris_items_count = Tracker.first.items.all.count
    @comment_count = Comment.all.count
    @user_count = User.all.count
    @latest_users = User.order(created_at: :desc).limit(10)
  end
  
  private
    def authorize_admin! 
      authenticate_user!
      unless current_user.admin?
        redirect_to root_path, alert: "You must be an admin to do that."
      end
    end
    
end
