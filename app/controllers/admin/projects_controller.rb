class Admin::ProjectsController < Admin::ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :archive, :destroy]
  def index
    # @projects = Project.excluding_archived.order(:created_at).page params[:page]
    # @projects = Project.all
    @projects = Project.all.order(:created_at).page params[:page]

  end
  
  def show
  end
  
  def new
    @project = Project.new
  end
  
  def create 
    @project = Project.new(user_params)
    
    if @project.save
      flash[:notice] = "Project has been created."
      redirect_to admin_projects_path
    else
      flash[:alert] = "Project has not been created."
      render "new"
    end
  end
  
  def edit
  end
  
  def update
    
    if @project.update(project_params)
      flash[:notice] = "Project has been updated."
      redirect_to admin_projects_path
    else
      flash[:alert] = "Project has not been updated."
      render  "edit"
    end
  end
  
  def destroy
    if @project.destroy
      flash[:notice] = "Project has been deleted."
      redirect_to admin_projects_path
    end
  end
  
  def archive

      @user.update(archived_at: Time.now)
      
      flash[:notice] = "Project has been archived."

    redirect_to admin_projects_path
  end
  
  private
    def project_params
      params.require(:project).permit(:name, :image, :cover_photo, :description, 
                                      :water_body, :lat, :lng, :body_of_water_name,
                                      :body_of_water_id, :tag_id,
                                      attachments_attributes: [:file, :file_cache])
    end
    
    def set_project
      @project = Project.find(params[:id])
    end
end


