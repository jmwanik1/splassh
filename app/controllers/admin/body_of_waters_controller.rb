class Admin::BodyOfWatersController < Admin::ApplicationController
  before_action :set_bow, only: [:edit, :update, :destroy]
  
  def index
    @bows = BodyOfWater.all.order(:created_at).page params[:page]
  end
  
  def new
    @bow = BodyOfWater.new
  end
  
  def create
    @bow = BodyOfWater.new(bow_params)
    
    if @bow.save
      flash[:notice] = "Body of Water has been created."
      redirect_to admin_body_of_waters_path
    else
      flash[:alert] = "Body of Water has not been created."
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @bow.update(bow_params)
      flash[:notice] = "Body of Water has been updated."
      redirect_to admin_body_of_waters_path
    else
      flash[:alert] = "Body of Water has not been updated."
      render "edit"
    end
  end
  
  def destroy
    if @bow.destroy
      flash[:notice] = "Body of Water has been deleted."
      redirect_to admin_states_path
    else
      flash[:alert] = "Body of Water has not been deleted."
    end
  end
  
  private 
    def set_bow
      @bow = BodyOfWater.find(params[:id])
    end
    
    def bow_params
      params.require(:body_of_water).permit(:name, :color, :desc)
    end
end
