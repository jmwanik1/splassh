module Ph
  def process_meta_data(dataset)
    # TODO: process all newly added dataset
    # Process the first dataset
    # process_dataset(SecooraDataset.last.dataset_url)
    # Retrive the data
    resource = RestClient.get dataset.url
    body = resource.body
    body = JSON.parse(body)
    
    # Extract data
    table = body["table"] 
    rows = table["rows"]
    columns = table["columnNames"]
    # Process data
    return update_meta_data(dataset, rows, columns)
  end
  
  def update_meta_data(dataset, rows, columns)
    flag = false
    # Location of attributes of interest
    attributes_loc = [columns.find_index("Attribute Name"), 
                      columns.find_index("Value")]
                      
    # Attritubutes of interest
    fields = ["cdm_timeseries_variables", "geospatial_lat_max", 
              "geospatial_lon_max", "time_coverage_start", "time_coverage_end"]
    
    # Get rows with attributes of interest and update dataset
    rows.each do |row|
      row_data = {}
      if fields.include? row[attributes_loc[0]]
        # put the metadata in a ruby hash for easy update
        row_data[row[attributes_loc[0]]] = row[attributes_loc[1]]
        puts row_data.inspect
        update_ph_data(dataset, row_data)? flag = true: flag = false
      end
    end
  end
  
  # Given a dataset and data hash, update the dataset
  def update_ph_data(dataset, data)
    if dataset.update(data)
      true
    else
      false
    end
  end
  
  # Given a url, create a request to the server and retrieve the data
  def retrieve_ph_data(dataset, start_date=nil, end_date=nil, initial_request)
    # in the event start and end dates are not given 
    if start_date.nil? or end_date.nil?
      unless dataset.time_coverage_start.nil?
        start_date = date_format(dataset.time_coverage_start)
        end_date = date_format(dataset.time_coverage_end)
      end
    end
    
    # assemble the url to make a request to
    url = url_assembler(dataset, start_date, end_date, initial_request)
    begin
      resource = RestClient.get url
    rescue
      resource = nil
    end
    
    # Process the url and return the data 
    data = process_resource(dataset, resource, initial_request)
    
    return data
  end
  
  # Given a resource, make a request to the server and retrieve the data
  def process_resource(dataset, resource, initial_request)
    data = nil 
    # Remove empty datasets 
    if resource.nil? and initial_request
      dataset.destroy
    else
      # Extract data
      body = resource.body
      body = JSON.parse(body)
      table = body["table"] 
      data = table["rows"]
      # if initial request, update correct start and end date of data
      # as indicated in returned data
      if initial_request
        update_start_end_date(dataset, data)
      end 
    end
    return data
  end
  
  # update the dataset based on the start and end date of the
  # given data
  def update_start_end_date(dataset, data)
    # The order of the data is such that the start date is 
    # the first array element and the end data is the last element
    start_date = data[data.length - 1][0]
    end_date   = data[0][0]
    
    # If dates are equal, add one extra day to end date
    s_date = Date.parse(start_date).strftime("%F")
    e_date = Date.parse(end_date).strftime("%F")
    
    if s_date.eql? e_date
      end_date = Date.parse(end_date)
      end_date += 1
      end_date = end_date.strftime("%FT%RZ")
    end
    
    date = {time_coverage_start: start_date, time_coverage_end: end_date}
    return update_ph_data(dataset, date)
  end
  
  def url_assembler(dataset, start_date, end_date, initial_request)
    url = dataset.url.gsub('info', 'tabledap') 
    url = url.gsub('/index.json', '.json') 
    variables = dataset.cdm_timeseries_variables.split(",")
    ph = variables.find_index("sea_water_ph_reported_on_total_scale") ||
         variables.find_index("sea_water_ph_reported_on_total_scale_surface")
    # assemble query based on the type of request
    url << query_assembler(initial_request, start_date, end_date, variables[ph])
    logger.debug url
    return url
  end
  
  def query_assembler(initial_request, start_date, end_date, variable)
    if initial_request
      "?time%2C#{variable}&time%3E=#{start_date}" +
      "&time%3c=#{end_date}&#{variable}!=NaN"
    else
      "?time%2C#{variable}&time%3E=#{start_date}&time%3C#{end_date}"
    end
  end
  
  def date_format(date)
    unless date.nil?
      DateTime.parse(date).strftime("%F")
    end
  end
end