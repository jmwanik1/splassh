module Secoora
  def secoora_data(url)
    # TODO: process all newly added dataset
    # Process the first dataset
    # process_dataset(SecooraDataset.last.dataset_url)
    # Retrive the data
    resource = RestClient.get url
    body = resource.body
    body = JSON.parse(body)
    
    # Extract data
    table = body["table"] 
    rows = table["rows"]
    columns = table["columnNames"]
    # Process data
    return process_dataset(rows, columns)
  end
  
  def process_dataset(rows, columns)
    flag = false
    # Data fields of interest
    
    # Find location of fields of interest in data table
    fields = [["Dataset ID", ], ["Title", ], ["Institution", ], ["Info", ]]
    for i in 0..fields.length - 1
      logger.debug fields.class
      fields[i][1] = columns.find_index(fields[i][0])
    end
    
    # Extract the fields of interest from data table
    rows.each do |row|
      row_data = {}
      for i in 0..fields.length - 1
        puts fields[i][0]
        id = fields[i][0].downcase.parameterize(separator: '_')
        row_data[id] = row[fields[i][1]]        
      end
      # We're only working with ph data, so we 
      # add the extracted data in database
      save_in_ph_data(row_data)? flag = true: flag = false
    end
    return flag
  end
  
  def save_in_ph_data(data)
    data["title"] = data["title"].titleize
    data["url"] = data["info"]
    data.delete "info"
    if PhDataset.create(data)
      true
    else
      false
    end
  end
end