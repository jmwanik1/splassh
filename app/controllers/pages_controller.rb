class PagesController < ApplicationController
  def home
    # Feature the admin projects and admin's favorited projects
    # on the homepage
    @carousel_featured_projects = User.first.favorite_projects.ord_recent.limit(3)
    # @featured_projects =  User.first.favorite_projects.ord_recent.limit(4).offset(3)
    
    # @carousel_featured_projects = User.first\qte_projects.limit(4).offset(3)
    
    
    
    @featured_projects = Project.all.ord_recent
    # @featured_projects =  Project.ord_recent.limit(4).offset(3)
    # @featured_projects =  Project.ord_recent.limit(4).offset(3)
    # @carousel_featured_projects = User.first.favorite_projects.where(id: [9, 4]).ord_recent.limit(4)
    # @featured_projects =  User.first.favorite_projects.where(id: [554,12,2,7]).ord_recent.limit(4)


  end

  def about
  end

  def team
    @members = Team.all.order(:created_at)
  end

  def contact
  end

  def faq
  end

  def donate
  end

  def sponsors
  end

  def terms
  end

  def privacy
  end
  
  def metrics
    @project_count = Project.all.count
    @debris_items_count = Tracker.first.items.all.count
    @comment_count = Comment.all.count
    @user_count = User.all.count
  end
  
  def testimonials
  end

end
