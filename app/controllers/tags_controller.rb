class TagsController < ApplicationController
  before_action :set_tag, only: [:show]
  def index
    @tags = @tags.all
  end
  
  def show
    @projects = @tag.projects.page params[:page]  
  end
  
  private
    def set_tag
      @tag = Tag.find(params[:id])
    end
end