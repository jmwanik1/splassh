class CommentsController < ApplicationController
  before_action :find_commentable

  def new
    @comment = Comment.new
  end

  def create
    @comment = @commentable.comments.new(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      create_notification
      track_activity @comment
      RenderCommentJob.perform_now(@comment, @commentable, current_user)
    else
      flash[:alert] = "Your comment was not saved."
      render :back
    end
  end
  
  def show 
    @comment = Comment.find(params[:id])
  end
  
  def destroy
    @comment = @commentable.comments.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to root_path,
        notice: "Comment has been deleted." }
      format.json { head :no_content }
      format.js
    end
  end
  
  private
    def comment_params
      params.require(:comment).permit(:content)
    end
  
  def find_commentable
    params.each do |name, value|
      if name =~ /(.+)_id$/
        @commentable = $1.classify.constantize.find(value)
      end
    end
  end
  
  def create_notification
    # Create notification to all users commenting on this project
    # except the user that commented 
    action = "#{current_user.username} posted a comment"
    (@commentable.users.uniq - [current_user]).each do |user|
        Notification.create(to: user, by: current_user, action: action,
                           notifiable: @comment)
                           
        # Broadcast the notification users 
        ActionCable.server.broadcast "user_notifications_#{user.id}",
              username: current_user.username,
              unread: user.notifications.unread.count,
              action: action,
              type: "a comment",
              url: project_comment_path(@commentable, @comment)
    end
  end
end
