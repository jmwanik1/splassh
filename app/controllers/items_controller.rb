class ItemsController < ApplicationController
  before_action :set_tracker
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :new, :update, :destroy]
  
  def new
    @item = @tracker.items.build
    3.times { @item.attachments.build }
  end
  
  def create
    @item = @tracker.items.build(item_params)
    @item.user = current_user
    if @item.save
      track_activity @item
      flash[:notice] = "Item has been created."
      redirect_to [@tracker, @item]
    else
      flash[:alert] = "Item has not been created."
      render "new"
    end
  end
  
  def show
    @status = @item.statuses.build(state_id: @item.state_id)
  end
  
  def edit
    if @tracker.user_id != current_user.id 
      flash[:alert] = "You are not allowed to edit this item."
      redirect_to [:manage, @tracker]
    end
  end
  
  def update
    if @item.update(item_params)
      track_activity @item
      flash[:notice] = "Item has been updated."
      redirect_to [:manage, @tracker]
    else
      flash[:notice] = "Item has not been update."
      render "edit"
    end
  end
  
  def destroy
    if @item.destroy
      flash[:notice] = "Item has been deleted."
      redirect_to [:manage, @tracker]
    else
      flash[:alert] = "Item has not deleted."
      redirect_to [:manage, @tracker]
    end
  end
  
  private
    def set_tracker
      @tracker = Tracker.find(params[:tracker_id])
    end
    
    def set_item
      @item = @tracker.items.find(params[:id])
    end
    
    def item_params
      params.require(:item).permit(:name, :description, :lat, :lng, 
                      attachments_attributes: [:file, :file_cache])
    end
end
