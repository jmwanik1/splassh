class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, 
                only: [:index, :new, :edit, :update, :destroy]
  
  def index
    @projects = Project.where(author: current_user).
                      order('created_at asc').page params[:page]
  end
  
  def show
    authorize @project, :show?
    load_show_instants
    # log impression
    log_impression
    # get activities performed on each instance
    
    # redirect if using an old slug url
    # If an old id or a numeric id was used to find the record, then
    # the request path will not match the project_path, and we should do
    # a 301 redirect that uses the current friendly id.
    if request.path != project_path(@project)
      return redirect_to @project, :status => :moved_permanently
    end
  end
  
  def new
    @project = Project.new
    3.times {@project.attachments.build}
  end
  
  def edit
    # Unauthorized to edit if not project owner
    if @project.author_id != current_user.id
      flash[:alert] = "You are not allowed to edit this project."
      redirect_to @project
    end
  end
  
  def update
    if @project.update(project_params)
      track_activity @project
      flash[:notice] = "Project has been updated."
      redirect_to @project
    else
      flash[:alert] = "Project has not been updated."
      render action: 'edit'
    end
  end
  
  def destroy
    if @project.destroy
      flash[:notice] = "Project has been deleted."
      redirect_to projects_path
    else
      flash[:alert] = "Project has not been deleted."
      redirect_to projects_path
    end
  end
  
  def create
    @project = Project.create(project_params)
    @project.author = current_user
    
    if @project.save
      flash[:notice] = "Project has been created."
      track_activity @project
      redirect_to @project
    else
      render action: 'new'
    end
  end
  
  def load_show_instants
    @scientific_data = @project.scientific_data.order('created_at desc').limit(10)
    @scientific_datum = ScientificDatum.new 
    @site = Site.new
    @categories = Category.all.order('name ASC')
    @chemicals = Chemical.all
  end
  
  private
    def project_params
      params.require(:project).permit(:name, :image, :cover_photo, :description, 
                                      :water_body, :lat, :lng, :body_of_water_name,
                                      :body_of_water_id, :tag_id,
                                      attachments_attributes: [:file, :file_cache])
    end
    
    def set_project
      @project = Project.friendly_id.find(params[:id])
      puts params[:id]
      logger.debug @project.to_json
      rescue ActiveRecord::RecordNotFound
        flash[:alert] = "The project you were looking for could not be found."
        redirect_to projects_path
    end
end
