class FieldNotesController < ActionController::Base
  before_action :set_project
  
  def new
    @field_note = FieldNote.new
  end
  
  def create
    @field_note = @project.field_notes.build(field_note_params)
    @field_note.user_id = current_user.id
    respond_to do |format|
      if @field_note.save
        format.html { redirect_to @project, 
                      notice: "Field note was created successfully" }
        format.json { render :show, status: :created, location: @project }
        format.js
      else
        format.html { render :new, alert: "Field note was note created successfully " }
        format.json { render json: @project, status: :unprocessable_entity }
        format.js
      end
    end
  end
  
  def destroy
    @field_note = @project.field_notes.find(params[:id])
    @field_note.destroy
    respond_to do |format|
      format.html { redirect_to @project,
        notice: "Field not has been destroyed" }
      format.json { head :no_content }
      format.js
    end
  end
  
  private 
    def field_note_params
        params.require(:field_note).permit(:note)
    end
    
    def set_project
      @project = Project.friendly_id.find(params[:project_id])
    end
end