class ExploreController < ApplicationController
  def index
    
    if params[:name].present?
      @name = params[:name]
      search = Search.new(@name)
      @tracker_search_results = Tracker.where(
          search.where_clause,
          search.where_args).
        order(search.order)
    else
      @trackers = Tracker.all
      @items = Item.desc.page params[:page]
    end
  
  
    if params[:name].present?
      @name = params[:name]
      search = Search.new(@name)
      @search_results = Project.where(
          search.where_clause,
          search.where_args).
        order(search.order)
    else
      @projects = Project.all
      # @trackers = Tracker.all
      @items = Item.desc.page params[:page]
    end
  
  end
  
  def self.search(title)
    where('Lower(title) LIKE :title', term: "%#{title.downcase}%")
  end
  
end
