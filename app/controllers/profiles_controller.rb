class ProfilesController < ApplicationController
  before_action :set_user, only: [:index, :edit, :new, :update, :create]
  before_action :set_profile, only: [:edit, :update]
  before_action :authenticate_user!, 
                only: [:new, :edit, :update, :create, :destroy]
  def index
    @activities = Kaminari.paginate_array(@user.activities.desc).page(params[:page]).per(10)
        

    if @user.profile.present? == false
      @profile = Profile.new
    end
  end
  
  def new
    @profile = Profile.new
  end
  
  def create
    @profile = Profile.create(profile_params)
    
    if @profile.save
      flash[:notice] = "Profile has been created."
      redirect_to user_profiles_path(@user)
    else
      flash[:alert] = "Profile has not been created."
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @user.profile.update(profile_params)
      flash[:notice] = "Profile has been updated."
      redirect_to user_profiles_path(@user)
    else
      flash[:alert] = "Profile has not been updated."
      render 'edit'
    end
  end
  
  private
    # def profile_params
    #   params.require(:profile).permit(:firstname, :lastname)
    # end

        def profile_params
      params.require(:profile).permit(:firstname, :lastname, :tag, 
                                      :about, :school, :educationlevel, :user_id, :private)
    end
    def set_user
      @user = User.find(params[:user_id])
      rescue ActiveRecord::RecordNotFound
        flash[:alert] = "The user you were looking for could not be found."
        redirect_to root_path
    end
    
    def set_profile
      @profile = @user.profile
    end
end
