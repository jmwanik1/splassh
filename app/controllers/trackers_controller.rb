class TrackersController < ApplicationController
  before_action :set_tracker, only: [:show, :edit, :update, :destroy, :manage]
  before_action :set_items, only: [:show, :manage]
  before_action :set_projects, only: [:new, :edit]
  before_action :authenticate_user!, 
                only: [:index, :new, :edit, :update, :destroy]
  before_action :authorize_user, only: [:manage]
  
  def index
    @trackers = Tracker.where(user: current_user).
                       order('created_at desc')
  end

  def new
    @tracker = Tracker.new
  end
  
  def create
    @tracker = Tracker.create(tracker_params)
    @tracker.user = current_user
    if @tracker.save
      flash[:notice] = "Tracker has been created."
      track_activity @tracker
      redirect_to @tracker
    else
      render "new"
    end
  end
  
  def show
  end
  
  def edit
    authorize
  end
  
  def update
    if @tracker.update(tracker_params)
      flash[:notice] = "Tracker has been updated."
      track_activity @tracker
      redirect_to @tracker
    else
      flash[:alert] = "Tracker has not been updated."
      render "edit"
    end
  end
  
  def destroy
    if @tracker.destroy
      flash[:notice] = "Tracker has been deleted."
      redirect_to trackers_path
    else
      flash[:alert] = "Tracker has not been deleted."
      redirect_to trackers_path
    end
  end
  
  def manage
  end
  
  private
    def tracker_params
      params.require(:tracker).permit(:name, :description, :project_id,
                                      :private, :lat, :lng)
    end
    
    def set_tracker
      @tracker = Tracker.friendly_id.find(params[:id])
      rescue ActiveRecord::RecordNotFound 
        flash[:alert] = "The tracker you were looking for could not be found."
        redirect_to trackers_path
    end
    
    def set_items
      @items = @tracker.items.desc.page params[:page]
    end
    
    def set_projects
      @projects = current_user.projects
    end
    
    def authorize
      # Unauthorized to edit if not tracker owner
      if @tracker.user != current_user
        flash[:alert] = "You are not allowed to do that."
        redirect_to trackers_path
      end
    end
    
    def authorize_user
      if @tracker.private? and @tracker.user != current_user
         flash[:alert] = "This is a private tracker."
         redirect_to trackers_path
      end
    end
end
