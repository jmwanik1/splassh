class ContributorsController < ApplicationController
  before_action :find_contributable
  
  def index
    if params[:email].present?
      @email = params[:email]
      search = Search.new(@email)
      @user = User.where(
          search.where_clause,
          search.where_args).
        order(search.order)
    end
    @users = User.excluding_archived.order(:created_at).page params[:page]
  end
  
  def self.search(title)
    where('Lower(title) LIKE :title', term: "%#{title.downcase}%")
  end
  
  def create
    @contributor = @contributable.contributors.new 
    user = User.find(params[:user])
    @contributor.user = user
    puts "hello"
    if @contributor.save
      track_activity @contributor
      flash[:notice] = "Contributor has been added."
      action = "#{current_user.username} added you as a contributor to #{@contributable.name}"
      Notification.create(to: user, by: current_user, action: action,
                           notifiable: @contributable)
                           
      # Broadcast the notification users 
      ActionCable.server.broadcast "user_notifications_#{user.id}",
            username: user.username,
            unread: user.notifications.unread.count,
            action: action,
            type: "a contributor",
            url: projects_path(@contributable)
      redirect_to project_contributors_path(@contributable)
    else
      flash[:alert] = "Contributor could not be added."
      redirect_to project_contributors_path(@contributable)
    end
  end
  
  def destroy
    @contributor = @contributable.contributors.find(params[:id])
    if @contributor.destroy
      flash[:notice] = "Contributor has been removed."
      redirect_to project_contributors_path(@contributable)
    else
      flash[:alert] = "Contributor could not be removed."
      redirect_to project_contributors_path(@contributable)
    end
  end
  
  private
  
    def find_contributable
      params.each do |name, value|
        if name =~ /(.+)_id$/
          @contributable = $1.classify.constantize.find(value)
        end
      end
    end
    
    def find_user
      @user = User.find_by_email(params[:email])
    end
end