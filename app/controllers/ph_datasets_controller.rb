class PhDatasetsController < ApplicationController
  before_action :authorize_admin!, 
                only: [:update_data, :clean_data, :destroy]
  include Ph
  def index
    @ph_datasets = PhDataset.order('title ASC')
    @datasets = Kaminari.paginate_array(@ph_datasets)
                        .page(params[:page]).per(10)
  end
  
  def destroy
    @dataset = PhDataset.find(id)
    if @dataset.destroy
      flash[:notice] = "Dataset has been deleted"
    else
      flash[:alert] = "Dataset could not be deleted"
    end
  end
  
  # Get the selected dataset
  def update_selection
    @dataset = PhDataset.find(params[:id])
    respond_to do |format|
      format.js
    end
  end
  
  # Retrieve and Update the meta data for the SECOORA dataset in the database
  def update_data
    datasets = PhDataset.all
    # 
    datasets.each do |dataset|
      process_meta_data(dataset)
    end
    # TODO: show success or failure message
    redirect_to ph_datasets_path
  end
  
  # Given a dataset retrieve the dataset from the erddap server
  def retrieve_data
    dataset_params    # permit params
    start_date = params[:ph_dataset][:time_coverage_start]
    end_date = params[:ph_dataset][:time_coverage_end]
    # Needed for corrrectly parsing the date'
    initial_request = false
    ph_dataset = PhDataset.find(params[:ph_dataset][:id])
    @data = retrieve_ph_data(ph_dataset,
                             Date.strptime(start_date, "%m/%d/%Y"),
                             Date.strptime(end_date, "%m/%d/%Y"),
                             initial_request)
    respond_to do |format|
      format.json { render json: @data}
    end
  end
  
  # Make request to ERDDAP server and findout which dataset will return data
  # if a dataset does not return data it is removed from database
  def clean_data
    # clean unclean data
    # default is the first dataset 
    CleanPhDataJob.perform_now(current_user)
    respond_to do |format|
      format.js
    end
  end
  
  private
    def dataset_params
      params.require(:ph_dataset).permit(:id, :time_coverage_start,
                                        :time_coverage_end)  
    end
    
    def authorize_admin! 
      authenticate_user!
      unless current_user.admin?
        redirect_to root_path, alert: "You must be an admin to do that."
      end
    end
end
