class StatusesController < ApplicationController
  before_action :set_item
  
  def create
    @status = @item.statuses.build(status_params)
    @status.author = current_user
    
    if @status.save 
      flash[:notice] = "Status or state has been created."
      redirect_to [@item.tracker, @item]
    else
      if @status.errors[:base]
        
        flash[:alert] = "#{@status.errors[:base]}"
      else
        flash[:alert] = "Status or state could not be created."
      end
      @tracker = @item.tracker
      redirect_to [@item.tracker, @item]
    end
  end
  
  private
    def set_item
      @item = Item.find(params[:item_id])      
    end
    
    def status_params
      params.require(:status).permit(:content, :state_id)
    end
end
