class DebrisController < ApplicationController
  before_action :set_debris, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, 
                only: [:new, :edit, :update, :destroy]
                
  # Page size constant
  PAGE_SIZE = 10
                
  def index
    #@debris_locations = Debri.all
    #@debris = Debri.all.limit(10)
    @page = (params[:page] || 0).to_i
    @debris = Debri.all.offset(PAGE_SIZE * @page).limit(PAGE_SIZE)
    @locations = @debris
  end
  
  def show
    @debris = Debri.find(params[:id])
    @comments = @debris.comments.limit(20).order('id DESC')
  end
  
  def new
    @debris = Debri.new
  end
  
  def create
    @debris = Debri.create(debris_params)
    if @debris.save
      flash[:notice] = "Debris has been successfully create."
      redirect_to @debris
    else
      flash[:alert] = "Debris has not been created."
      render 'new'
    end
  end
  
  def destroy
    if @debris.destroy
      flash[:notice] = "Debris has been deleted."
      redirect_to debris_path
    else
      flash[:alert] = "Debris has not been deleted."
      redirect_to debris_path
    end
  end
  
  private
    def debris_params
      params.require(:debri).permit(:title, :status, :description, :lat, :lng)
    end
    
    def set_debris
      @debris = Debri.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:alert] = "The debris you were looking for could not be found."
        redirect_to debris_path
      end
end
