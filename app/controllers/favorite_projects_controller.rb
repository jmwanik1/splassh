class FavoriteProjectsController < ApplicationController
  before_action :set_project
  
  def create
    @favorite = Favorite.create(favorited: @project, user: current_user)
    if @favorite.save
      track_activity @favorite
      redirect_to @project
      flash[:notice] = "Project has been favorited."
    else
      flash[:alert] = "Projet has not been favorited."
    end
  end
  
  def destroy
    @favorite = Favorite.where(favorited_id: @project.id, 
                               user_id: current_user.id).first
    if @favorite.destroy
      flash[:notice] = "Project has been removed from favorites"
      redirect_to @project
    else  
      flash[:alert] = "Project has not been removed from favorites"
      redirect_to @project
    end
  end
  
  private
    
    def set_project
      @project = Project.find(params[:project_id] || params[:id])
    end
end