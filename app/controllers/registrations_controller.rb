class RegistrationsController < Devise::RegistrationsController
  private
  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation,
                                 profile_attributes: [:firstname, :lastname])
  end

  def account_update_params
    params.require(:user).permit(:name, :email, :password, 
                                 :password_confirmation, :current_password, 
                                 profile_attributes: [:firstname, :lastname])
  end
end