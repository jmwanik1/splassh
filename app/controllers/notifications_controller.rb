class NotificationsController < ApplicationController
  before_action :authenticate_user!, :set_notifications
  
  def index
  end
  
  def show
    
  end
  
  def mark_as_read
    # Notifications will be deleted when the user reads them
    # to preserve notifications, uncomment the line below
    # and comment on the delete line
    # @notifications.update_all(read_at: Time.zone.now)
    @notifications.delete_all
  end
  
  private
    def set_notifications
      @notifications = Notification.where(to: current_user).unread 
    end
end
