class SitesController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :destroyw]
  before_action :set_project, only: [:new, :create, :edit, :show, :destroy]
  before_action :set_site, only: [:show, :edit, :destroy]
  
  def index
    @site = Site.where(params[:project_id])
  end
  
  def new
    @site = @project.site.build
  end
  
  def create
    @site = @project.site.build(site_params)
    @site.project = @project
    @site.user = current_user
    
    # if @site.save
    #   flash[:notice] = "Project site has been created."
    #   track_activity @site
    #   redirect_to @project
    # else
    #   render 'new'
    # end
    respond_to do |format|
      if @site.save
        track_activity @site
        format.html { redirect_to [@project, @site], 
                      notice: "Site was added successfully." }
        format.json { render :show, status: :created, location: @site }
        format.js
      else
        format.html { render :new, alert: "Site could not be added. " }
        format.json { render json: @site, status: :unprocessable_entity }
        format.js
      end
    end
  end
  
  def edit
  end

  def update
    @site = Site.find(params[:id])
    @site.project = Project.find(params[:project_id])
    respond_to do |format|
      if @site.update(site_params)
        track_activity @site
        format.html { redirect_to project_site_url(@site.project.id, @site.id), 
                      notice: "The site was successfully update." }
        format.json { render :show, status: :ok, location: @site }
        format.js
      else
        format.html { render :update,
                      notice: "The site could not be updated." }
        format.json { render json: @site.errors,
                      status: :unprocessable_entity  }
        format.js
      end
    end
  end

  def destroy
    projectID = Project.find(params[:project_id])
    puts projectID
    puts "deLETETEEETETE"
    @site.destroy
    respond_to do |format|
      format.html { redirect_to @project,
                    notice: "Site has been deleted." }
      format.json { head :no_content }
      format.js
    end
  end
  
  # def destroy
  #   if @site.destroy
  #     flash[:notice] = "Site has been deleted."
  #     # redirect_to trackers_path
  #   else
  #     flash[:alert] = "Site has not been deleted."
  #     # redirect_to trackers_path
  #   end
  # end
  
  
    
  private
  def site_params
    params.require(:site).permit(:name, :description, :lat, :lng)
  end
  
  def set_site
    @site = @project.site.find(params[:id])
  end
  
  def set_project
    @project = Project.find(params[:project_id])
    puts @project.id
  end
end
