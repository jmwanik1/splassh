class RenderCommentJob < ApplicationJob
  queue_as :default

  def perform(comment, commentable, current_user)
    # Do something later
    renderer = ApplicationController.renderer.new
    page_content = renderer.render(partial: 'comments/comment', 
                           locals: {comment: comment, commentable: commentable,
                              current_user: current_user,
                               # In this case, we don't want to render nested comments
                               # because we don't have access to warden
                               can_render: false
                           })
    
    ActionCable.server.broadcast "project:#{commentable.id}:comments",
                                  commentable_id: commentable.id,
                                  comment: page_content
  end
end
