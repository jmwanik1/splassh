class CleanPhDataJob < ApplicationJob
  queue_as :default
  include Ph

  def perform(current_user, *args)
    clean_up(current_user)
  end
  
  def clean_up(current_user)
    flag = true
    # clean unclean data
    # default is the first dataset 
    if flag and not SecooraDataset.first.cleaned?
      datasets = PhDataset.all
      current = 1
      total = datasets.count
      datasets.each do |dataset|
        # retrieve and update dataset
        # true for initial request
        retrieve_ph_data(dataset, true)
        broadcast_progress(current_user, current, total)
        current += 1
      end
      @notice = "Dataset has been cleaned"
      # prevent updating twice
      flag = false
    else
      @notice = "Dataset already cleaned"
    end
    # mark dataset as cleaned 
    mark_cleaned(SecooraDataset.first)
    # Notify user about 
    notify_finished(current_user)
  end
  
  def broadcast_progress(current_user, curr, tot)
    # indicate progress
    ActionCable.server.broadcast "secoora:user:#{current_user.id}",
      current: curr,
      total: tot
  end
  
  def notify_finished(current_user)
    Notification.create(to: current_user, by: current_user, 
                        action: "finished processing ph data",
                        notifiable: nil)
    ActionCable.server.broadcast "user_notifications_#{current_user.id}",
              username: current_user.username,
              unread: current_user.notifications.unread.count,
              action: "finished processing ph data",
              type: "dataset",
              url: "#"
  end
  
  def mark_cleaned(secoora_dataset)
    secoora_dataset.update(cleaned: true)
  end
  
end
