module TrackersHelper
  def tracker_owner?(tracker)
    user_signed_in? && current_user == tracker.user
  end
  
  def user_items
    @tracker.items.select { |item| item.user == current_user}    
  end
end
