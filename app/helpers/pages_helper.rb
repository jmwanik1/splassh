module PagesHelper
  def featured
    @projects = Project.all.limit(8)
  end
  
  def homepage
    "homepage" if current_page?(root_path)
  end
  
  def featured_first(index)
    if index == 0
      "active"
    end
  end
end
