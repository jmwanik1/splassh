module ProfilesHelper
  def profile_owner(user)
    user_signed_in? and current_user == user
  end
end
