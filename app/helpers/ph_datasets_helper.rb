module PhDatasetsHelper
  def url_assembler(dataset)
    url = dataset.url.gsub('info', 'tabledap') 
    url = url.gsub('/index.json', '.json') 
    variables = dataset.cdm_timeseries_variables.split(",")
    ph = variables.find_index("sea_water_ph_reported_on_total_scale") ||
         variables.find_index("sea_water_ph_reported_on_total_scale_surface")
    url << "?#{variables[ph]}&time%3E=" + 
          "#{date_format(dataset.time_coverage_start)}" +
          "&time%3c=#{date_format(dataset.time_coverage_end)}"
    return url
  end
  
  def date_format(date)
     DateTime.parse(date).strftime("%F")
  end
end
