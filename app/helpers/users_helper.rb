module UsersHelper
  # Return gravatar for the given user
  def gravatar_for(user, class_type)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: user.email, class: "#{class_type}")
  end
  
  def current_user?(user)
    current_user == user
  end
end