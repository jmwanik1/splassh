module ItemsHelper
  def state_transition_for(status)
    if status.previous_state != status.state
      content_tag(:p) do
        value = "<strong><i class='fa fa-gear'></i> state changed </strong>"
        if status.previous_state.present?
          value += " from #{render status.previous_state} "
        end
        
        value += " to #{render status.state}"
        value.html_safe
      end
    end
  end
  
  def item_owner?(item)
    user_signed_in? && current_user == item.user
  end
  
end
