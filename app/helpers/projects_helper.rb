module ProjectsHelper
  def project_owner?(project)
    user_signed_in? && current_user.id == project.author_id
  end
  
  def project_contributor?(project)
    user_signed_in? && 
    project.contributors.exists?(user_id: current_user.id)
  end
  
  def owner(project)
    " #{User.find(project.author_id).profile.firstname} 
      #{User.find(project.author_id).profile.lastname } "
  end
end
