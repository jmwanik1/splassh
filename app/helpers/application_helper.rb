module ApplicationHelper
  def title(*parts)
    unless parts.empty?
      content_for :title do
        (parts << "SPLASSH").join(" - ")
      end
    end
  end
  
  def alert_icon(key)
    case key
    when "notice"
      "info"
    when "success"
      "check"
    when "alert"
      "ban"
    end
  end
  
  def admins_only(&block)
    block.call if user_signed_in? && current_user.try(:admin?)
  end
  
  def site_users_only(&block)
    block.call if user_signed_in? && !current_user.try(:admin?)
  end
  
  def has_profile(user=current_user)
    user.profile.present?
  end
  
  def has_email(user=current_user)
    user.profile.present?
    "#{current_user.email}" 
  end
  
  def remove_mail_address(user)
    user_email = user[/[^@]+/]    
    "#{user_email.capitalize}" 
  end

  def user_name(user=current_user)
    "#{user.profile.firstname} #{user.profile.lastname}" 
  end
  
  def contributor_user_name(user)
    "#{user.firstname} #{user.lastname}" 
  end
  
  def default_tracker
    Tracker.first
  end
  
  def truncate_text(text)
    if text.is_a?(String) 
      text[0..250].gsub(/\s\w+\s*$/, '...')
    end
  end
end
