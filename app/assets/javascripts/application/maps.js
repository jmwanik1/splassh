// Google maps
var locations = {};
var markers = {};
var infoWindow = null;
var other_locations;
var other_markers;

function gmap_show(project, other_locations) {
  console.log(project);
  this.other_locations = other_locations;
  handler = Gmaps.build('Google');    // Initialize map
  if (other_locations == null) {
    if ((project.lat == null) || (project.lng == null) ) {    // check if coords are there
      return 0;
    }
    
    console.log("Work")
    
    handler.buildMap({ provider: {}, internal: {id: 'map'}}, function(){
      var title = project.body_of_water_name;
      if(typeof title == 'undefined') {
        title = project.title;
      }
      
      mark = handler.addMarkers([    // place marker
        {
          "lat": project.lat,    // project coordinates
          "lng": project.lng,
          "infowindow": "<b>" + project.name + "</b> "
        }
      ]);
      handler.bounds.extendWith(mark);
      handler.fitMapToBounds();
      handler.getMap().setZoom(12);    // Default map zoom
    });
  }
  else if (other_locations !== null) {
    console.log(other_locations.name);
    handler.buildMap({ internal: {id: 'map'}}, function(){
      mark = handler.addMarkers([    // place marker
        {
          "lat": other_locations.lat,    // project coordinates
          "lng": other_locations.lng,
          "infowindow": "<b>" + other_locations.name + "</b> "
        }
      ]);
      handler.bounds.extendWith(mark);
      handler.fitMapToBounds();
      handler.getMap().setZoom(12);    // Default map zoom
    });
    
    
    handler.currentInfowindow();
  }
}

var markers = [];
var map, infoWindow;

// Google maps form
function initAutocomplete() {
  
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 34.038176, lng: -84.582097},
    zoom: 2,
  });
  
  infoWindow = new google.maps.InfoWindow;
  
  // Try HTML5 geolocation.
  // setTimeout(function(){ 
    if(document.getElementById('project_lat') != null) {
      if (document.getElementById('project_lat').value != "" || document.getElementById('project_lng').value != "") {
        var pos = {
          lat: parseFloat(document.getElementById('project_lat').value),
          lng: parseFloat(document.getElementById('project_lng').value)
        };
  
        addMarker(pos, true);
        infoWindow.setPosition(pos);
        infoWindow.setContent('Location found.');
        infoWindow.open(map);
        map.setCenter(pos);
      } 
    }
    else if (document.getElementById('site_lat') != null) {
      if (document.getElementById('site_lat').value != "" || document.getElementById('site_lng').value != "") {
        var pos = {
          lat: parseFloat(document.getElementById('site_lat').value),
          lng: parseFloat(document.getElementById('site_lng').value)
        };
  
        addMarker(pos, true);
        infoWindow.setPosition(pos);
        infoWindow.setContent('Location found.');
        infoWindow.open(map);
        map.setCenter(pos);
      } 
    }
    else if (document.getElementById('tracker_lat') != null) {
      if (document.getElementById('tracker_lat').value != "" || document.getElementById('tracker_lng').value != "") {
        var pos = {
          lat: parseFloat(document.getElementById('tracker_lat').value),
          lng: parseFloat(document.getElementById('tracker_lng').value)
        };
  
        addMarker(pos, true);
        infoWindow.setPosition(pos);
        infoWindow.setContent('Location found.');
        infoWindow.open(map);
        map.setCenter(pos);
      } 
    }
    else {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
    
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          addMarker(pos, true);
          infoWindow.setPosition(pos);
          infoWindow.setContent('Location found.');
          infoWindow.open(map);
          map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }
  // }, 3000);

  // Create the search box and link it to the UI element.
  var input = document.getElementById('loc-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

 // This event listener will call addMarker() when the map is clicked.
  map.addListener('click', function(event) {
    infoWindow.close();
    clearMarkers();
    addMarker(event.latLng, false);
  });
  
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      
      // check which page the map is currently on
      var project_page = document.getElementById("project_lat");
      var tracker_page = document.getElementById("tracker_lat");
      var site_page = document.getElementById("site_lat");

      var item_page = document.getElementById("item_lat");
      
      if(project_page != null) {
        // set coords for project
        document.getElementById("project_lat").value = place.geometry.location.lat();
        document.getElementById("project_lng").value = place.geometry.location.lng();
      }
      else if(tracker_page != null) {
        //  set coordinates for tracker
        document.getElementById("tracker_lat").value = place.geometry.location.lat();
        document.getElementById("tracker_lng").value = place.geometry.location.lng();
      }
      else if(site_page != null) {
        //  set coordinates for site
        document.getElementById("site_lat").value = place.geometry.location.lat();
        document.getElementById("site_lng").value = place.geometry.location.lng();
      }
      else if(item_page != null) {
        //  set coordinates for tracker
        document.getElementById("item_lat").value = place.geometry.location.lat();
        document.getElementById("item_lng").value = place.geometry.location.lng();
      }
      
      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    
    map.fitBounds(bounds);
  });
}

// Adds a marker to the map and push to the array.
function addMarker(location, geolocated) {
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
  
  markers.push(marker);
  // set coords for input 
  var project_page = document.getElementById("project_lat");
  var tracker_page = document.getElementById("tracker_lat");
  var site_page = document.getElementById("site_lat");
  var item_page = document.getElementById("item_lat");
  
  var lat, lng;
  if (geolocated){
    lat = location.lat;
    lng = location.lng;
  } else {
    lat = location.lat();
    lng = location.lng();
  }
  
  
  if(project_page != null) {
    document.getElementById("project_lat").value = lat;
    document.getElementById("project_lng").value = lng;
  } 
  else if(tracker_page != null) {
    document.getElementById("tracker_lat").value = lat;
    document.getElementById("tracker_lng").value = lng;
  }
  else if(site_page != null) {
    document.getElementById("site_lat").value = lat;
    document.getElementById("site_lng").value = lng;
  }
  else if(item_page != null) {
    document.getElementById("item_lat").value = lat;
    document.getElementById("item_lng").value = lng;
  }
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}
// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

function makerLink(id) {
  // body...
  
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}

// show secoora ph datasets on map
function secoora_gmap_show(datasets) {
  var secoora_map = new google.maps.Map(document.getElementById('secoora_mapview'),{
    center: {lat: 30.790276, lng: -81.863893},
    zoom: 6
  });
  
  makers = [];
  
  var x = "";
  var y = "";
  for(var i in datasets) {
    // set pincolor for the marke
    x = parseFloat(datasets[i].geospatial_lat_max);
    y = parseFloat(datasets[i].geospatial_lon_max);
    var key = datasets[i].id;
    locations[key] = {
      index: i,
      lat: x,
      lng: y,
      institution: datasets[i].institution,
      title: datasets[i].title,
      created_at: datasets[i].time_coverage_start,
      updated_at: datasets[i].time_coverage_end
    }
  }
  
  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  infoWindow = new google.maps.InfoWindow({});
  for(var i in locations) {
    // Create a marker 
    var marker = new google.maps.Marker({
      position: {lat: locations[i].lat, lng: locations[i].lng},
      map: secoora_map,
      label: locations[i].label,
      zIndex: Math.round(locations[i].lat * -100000) << 5
    });
    
    // Add a listen to the map when it's clicked, open the info box
    google.maps.event.addListener(marker, 'click', (function(marker, i){
      return function() {
        infoWindow.setContent(
          "<p><b>Dataset Name: </b>" + locations[i].title + "</p>" +
          "<p><b>Institution: </b>" + locations[i].institution + "</p>" +
          "<p><b>Create Date: </b>" + locations[i].created_at + "</p>" +
          "<p><b>Update Date: </b>" + locations[i].updated_at + "</p>"
        );
        infoWindow.open(secoora_map, marker);
        secoora_map.setZoom(13);
        secoora_map.setCenter(marker.getPosition());
      }
    })(marker, i));
    
    // Add a listen to the marker to allow the infobox to be opened outside
    // the map
    markers[i] = marker;
    
    google.maps.event.addListener(secoora_map, 'click', function() {
      infoWindow.close();
    });
  }
}

function openInfoWindow(id) {
  google.maps.event.trigger(markers[id], 'click');
}


function displaySecooraMap(datasets) {
  document.getElementById('secoora_mapview').style.display="block";
  setTimeout(function () { secoora_gmap_show(datasets); }, 300); 
}


function toggleMapVew(id) {
  $('.nav-tabs a[href="#map_view"]').tab('show');
  openInfoWindow(id);
}


