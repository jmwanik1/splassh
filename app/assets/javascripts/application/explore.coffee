$(document).on 'turbolinks:load', ->
  showSearchResults()
  showTrackerSearchResults()
  
showSearchResults = () ->
  $('#search-form').on 'submit', ->
    $( "a.show_search_results" ).trigger( "click" );
    
showTrackerSearchResults = () ->
  $('#tracker-search-form').on 'submit', ->
    $( "a.show_tracker_search_results" ).trigger( "click" );