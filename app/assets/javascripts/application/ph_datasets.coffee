# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  # Update selection on load - Default 
  $(document).on 'turbolinks:load', (evt) ->
      
      # Object to store app variables 
      window.PH = {}
      window.invoke_data_retrieval = invoke_data_retrieval
      # Chart object for creating statistical charts 
      PH.Charts = new Charts 
      # Array object to hold retrieved data info
      PH.retrieved_data_info = {}
      PH.data = {}
      #hide chart options
      $('#chart_options').hide()
      $("#dataset_info").hide()
      $("#retrieved_dataset").hide()
      $("#available_charts").hide()
      $("#main_chart").hide()
      $("#changeTitle").click ->
        console.log("Clicked")
        title = $("#chartTitle").val()
        console.log(title)
        Highcharts.charts[Highcharts.charts.length - 1].setTitle({ text: title });
      
  # Update selection based on user selection 
  $(document).on 'change', '#dataset-select', (evt) ->
    $("#dataset_info").show()
    update_selection(evt)
  
  # Remove dataset from retrieved datasets
  $('ul#retrieved-datasets').on 'click', 'li', ->
    remove_dataset(@dataset.id)
    @.remove()
    
  # form date picker
  $('.date-picker').datepicker autoclose: true  
  # ======================== Charts ====================================
  # TODO: Refactor 
  # Charts listeners 
  # line chart
  $('#line-chart').click ->
    if PH.data != undefined
      data_info = $('#ph-data-info').data('url')
      data_info['start_date'] = new Date($("#start-date").val())
      PH.Charts.spline(PH.data, PH.retrieved_data_info)
      $("#main_chart").show()
      $("#chart_options").show()
    else
      retrieve_data_notification()
      
  # area chart
  $('#area-chart').click ->
    if PH.data != undefined
      data_info = $('#ph-data-info').data('url')
      data_info['start_date'] = new Date($("#start-date").val())
      PH.Charts.spline_area(PH.data, PH.retrieved_data_info)
      $("#main_chart").show()
      $("#chart_options").show()
    else
      retrieve_data_notification()
  
  # area chart - zoomable
  $('#area-chart-zoomable').click ->
    if PH.data != undefined
      data_info = $('#ph-data-info').data('url')
      data_info['start_date'] = new Date($("#start-date").val())
      PH.Charts.area_zoomable(PH.data, PH.retrieved_data_info)
      $("#main_chart").show()
      $("#chart_options").show()
    else
      retrieve_data_notification()
  
  # column chart 
  $('#column-chart').click ->
    if PH.data != undefined
      data_info = $('#ph-data-info').data('url')
      data_info['start_date'] = new Date($("#start-date").val())
      PH.Charts.bar_and_column(PH.data, PH.retrieved_data_info, 'column')
      $("#main_chart").show()
      $("#chart_options").show()
    else
      retrieve_data_notification()
      
  # bar  chart 
  $('#bar-chart').click ->
    if PH.data != undefined
      data_info = $('#ph-data-info').data('url')
      data_info['start_date'] = new Date($("#start-date").val())
      PH.Charts.bar_and_column(PH.data, PH.retrieved_data_info, 'bar')
      $("#main_chart").show()
      $("#chart_options").show()
    else
      retrieve_data_notification()
  
  # pie chart 
  $('#pie-chart').click ->
    if PH.data != undefined
      data_info = $('#ph-data-info').data('url')
      data_info['start_date'] = new Date($("#start-date").val())
      PH.Charts.pie(PH.data, PH.retrieved_data_info)
      $("#main_chart").show()
      $("#chart_options").show()
    else
      retrieve_data_notification()
      
  # pie chart 
  $('#scatter-plot').click ->
    if PH.data != undefined
      data_info = $('#ph-data-info').data('url')
      data_info['start_date'] = new Date($("#start-date").val())
      PH.Charts.scatter_plot(PH.data, PH.retrieved_data_info)
      $("#main_chart").show()
      $("#chart_options").show()
    else
      retrieve_data_notification()
  # ======================== End Charts ===================================
  
# ======================== Helper Methods =================================  
# Retrieve data when user clicks retrieve button
invoke_data_retrieval = () -> 
  console.log("retrieve data")
  $("#retrieved_dataset").show()
  $("#available_charts").show()
  data_info = $('#ph-data-info').data('url')
  if PH.retrieved_data_info.hasOwnProperty(data_info.dataset_id)
    swal("Data Retrieved!", "The data is available for use. Select it from the
         list of retrieved dataset(s)!", "info")
  else
    retrieve_data()
    add_retrieved_data_info()
# Makes a request to the ph controller and retrieves
# metadata on the select dataset
update_selection = (evt) ->
  # change status for indication
  $(".status").removeClass('red');
  $(".status").addClass('yellow');
  $(".box-header").children("i").text(" Dataset Selected");
  $.ajax 'update_selection',
    type: 'GET'
    dataType: 'script'
    data: {
      id: $("#dataset-select option:selected").val(),
      startDate: $("#start-date").val(),
      endDate: $("#end-date").val()
    }
    error: (jqXHR, textStatus, errorThrown) ->
    success: (data, textStatus, jqXHR) ->
      
retrieve_data = () ->
  show_loading()
  $.ajax 'retrieve_data',
    type: 'GET'
    dataType: 'script'
    data: 
      ph_dataset:
        id: $('#dataset-select option:selected').val()
        time_coverage_start: $('#start-date').val()
        time_coverage_end: $('#end-date').val()
    error: (jqXHR, textStatus, errorThrown) ->
      show_failure()
    success: (data, textStatus, jqXHR) -> 
      show_success()
      handle_success(data)
      
add_retrieved_data_info = () ->
  data_info = $('#ph-data-info').data('url')
  PH.retrieved_data_info[data_info.dataset_id] = data_info
  
  
handle_success = (data) ->
  data_info = $('#ph-data-info').data('url')
  PH.data[data_info.dataset_id] = JSON.parse(data)
  ph_data = PH.data[data_info.dataset_id].reverse()
  # filter null values 
  ph_data = ph_data.filter (x) -> (x != null and x != undefined)
  # convert ph_data date to utc format
  ph_data_date_to_utc(ph_data)
  # add data to retrieved data array
  $("#retrieved-datasets").empty()
  for key of PH.retrieved_data_info
    if PH.retrieved_data_info.hasOwnProperty(key)
      $("#retrieved-datasets").append "<li class='treeView list-group-item' 
        data-id='#{key}'>
        <a href='#' data-remote='true'> 
          #{PH.retrieved_data_info[key].title}
          <i class='fa fa-trash-o pull-right' aria-hidden='true'></i>
        </a>
      </li>"  
      
      
show_loading = () ->
  swal
    title: 'Retrieving Data!'
    text: 'Please wait while we retrieve data from SECOORA'
    type: 'info'
    showConfirmButton: false
  swal.showLoading()

show_success = () -> 
  $(".status").removeClass("yellow");
  $(".status").addClass("green");
  $(".box-header").children("i").text(" Dataset Retrieved");
  swal("Data Retrieved!", "The requested data has been retrieved from SECOORA!", "success");
  
show_failure = () -> 
  $(".status").removeClass("yellow");
  $(".status").addClass("green");
  swal("Error!", "An error occured while retrieving the data. Check the dates" +
                 " or try again later", 
      "error");

retrieve_data_notification = () ->
  swal("Retrieve Data!", "Please click \"Retrieve Data\" to request data from SECOORA!", 
       "info")

ph_data_date_to_utc = (ph_data) ->
  i = 0
  while i < ph_data.length
    date = new Date(ph_data[i][0])
    date = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), 
                    date.getUTCDate(), date.getUTCHours(), 
                    date.getUTCMinutes(), date.getUTCSeconds())
    ph_data[i][0] = date
    i++

# Remove the property(data) from the PH dataset and 
# retrieved data info objects given the dataset id
remove_dataset = (dataset_id) ->
  delete PH.data[dataset_id]
  delete PH.retrieved_data_info[dataset_id]
# ======================== End Helper Methods ===============================