# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'turbolinks:load', ->
  student = $("#profile-form input:radio[name=student]")
  yes_student = $("#profile-form input:radio[name=student][value='yes']")
  not_student = $("#profile-form input:radio[name=student][value='no']")
  
  education_level = $("#profile-form input:radio[name='profile[educationlevel]']")
  education = $("#profile-form #education")
  school = $("#profile-form #school")
  
  occupation = $("#profile-form #occupation")
  occupation_field = $('input#profile_tag')
  
  
  if occupation_field.val() == 'Student'
    yes_student.prop("checked", true)
  else
    not_student.prop("checked", true)
  
  student.change -> 
    value = this.value
    occupation.addClass('hidden')
    
    if value == 'no' 
      occupation.removeClass('hidden')
      education.addClass('hidden')
      school.addClass('hidden')
    else if value == 'yes'
      occupation_field.val('Student')
      education.removeClass('hidden')
      school.removeClass('hidden')