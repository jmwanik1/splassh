# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
cookie_id = undefined
$(document).on 'turbolinks:load', ->
  cookie_id = $("span#contributor").data()
  if cookie_id != undefined
    cookie_id = cookie_id.id

$(".pages.project").ready ->
  showContributorNotification(cookie_id)
   

showContributorNotification = (cookie_id) ->
  if cookie_id != undefined
    if not ($.cookie("splassh_project_#{cookie_id}"))
      swal 'You\'re a contributor!', 
            'As a contributor you can contribute by adding
            data to this project!', 'info'
      $.cookie("splassh_project_#{cookie_id}", '1', { expires: 45})