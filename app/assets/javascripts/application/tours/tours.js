// Instance the tour
/* global Tour */
/* global $ */

var tour;
function init() {
  tour.init();
}
$(document).on('turbolinks:load', function() {
  tour = new Tour();
  tour.addStep({
    element: "#step_one",
    title: "Data Source",
    content: "Southeast Coastal Ocean Observing Regional Association – " +
             " is the coastal ocean observing system for the Southeast U.S." +
             " This is the data source, visit secoora.org to learn more."
  });
  tour.addStep({
    element: "#step_two",
    title: "Dataset Window",
    content: "Use this tab to retrieve data and create charts"
  });
  tour.addStep({
    element: "#step_three",
    title: "Map Window",
    content: "Use this tab to view the location of selected dataset, including" +
             " all available dataset"
  });
  tour.addStep({
    element: "#step_four",
    title: "Dataset Retriever",
    content: "This forms allows you to retrieve data. It is a multistep form, where " + 
             " additional options are available once you select a dataset to retrieve." +
             " Please select a dataset to allow more steps of the demo. " + 
             "Dataset Suggestion: East Bay Bottom: "
  });
  tour.addStep({
    element: "#step_five",
    title: "Dataset Retriever",
    content: "This indicator shows the status of the selected dataset. There are three states: " +
             " Red: no dataset selected, Yellow: Dataset selected but not retrieved, and " +
             " Green: Dataset retrieved successfully."
  });
});

function startDemo(step) {
  // start the tour
  tour.restart();
  setTimeout(function() {
    tour.init(true);
    tour.restart();
    tour.setCurrentStep(step);
    tour.start(true);
  }, 100);
}

function updateSelectionDemo() {
  tour.addStep({
    element: "#date",
    title: "Date range picker",
    content: "This box and the one below allow you to retrieve data within a " +
             "set range. The allowed minimum and maximum dates are pre filled." 
    
  });
  tour.addStep({
    element: "#retrieve-data",
    title: "Retrieve",
    content: "Once you have set the date range. Click this button to retrieve the data."
  });
  tour.addStep({
    element: "#step_eight",
    title: "Retrieved Dataset",
    content: "This is a list of all dataset you have retrieved. You can remove a dataset " +
             " from this list by simply clicking on the trash icon."
  });
  tour.addStep({
    element: "#available_charts",
    title: "Available Charts",
    content: "Finally, these are the charts available to graph your retrieved data"
  });
}