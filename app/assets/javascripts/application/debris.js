

var markers = [];
var openInfo;
  function initMap(positions) {
    var infoWindow = new google.maps.InfoWindow({});
    
    var map = new google.maps.Map(document.getElementById('map'), {
       center: {lat: 34.038176, lng: -84.582097},
       zoom: 3,
    });
    
    var locations = [];
    for(var i in positions) {
      locations.push({
        lat: parseFloat(positions[i].lat), 
        lng: parseFloat(positions[i].lng),
        label: positions[i].id.toString(),
        title: positions[i].title,
        status: positions[i].status,
        created_at: positions[i].created_at,
        updated_at: positions[i].updated_at
       })
    }
  
    // Add some markers to the map.
    // Note: The code uses the JavaScript Array.prototype.map() method to
    // create an array of markers based on a given "locations" array.
    // The map() method here has nothing to do with the Google Maps API.
    for(var i in locations) {
      var marker = new google.maps.Marker({
        position: locations[i],
        map: map,
        label: locations[i].label,
        zIndex: Math.round(locations[i].lat * -100000) << 5
      });
      
      google.maps.event.addListener(marker, 'click', (function(marker, i){
        return function() {
          infoWindow.setContent(
              "<p><b>Type: </b>" + locations[i].title + "</p>" +
              "<p><b>Status: </b>" + locations[i].status + "<p>" + 
              "<p><b>Record Date: </b>" + locations[i].created_at + "</p>" +
              "<p><b>Ucpdate Date: </b>" + locations[i].updated_at + "</p>"
            );
          infoWindow.open(map, marker);
          map.setZoom(12);
          map.setCenter(marker.getPosition());
        }
      })(marker, i));
      
      
      markers.push(marker);
      
     google.maps.event.addListener(map, 'click', function() {
          infoWindow.close();
      });
    }
    
    // The function to trigger the marker click, 'id' is the reference index to the 'markers' array.
     openInfo = function(id){
        google.maps.event.trigger(markers[id], 'click', function() {
          infoWindow.open(map, markers[id]);
          map.setZoom(12);
          map.setCenter(marker[id].getPosition());
        });
    }
  }
  
 
  
 