$(document).on 'turbolinks:load', ->
  show_hide_alert()
  set_active()
  set_swal_defaults()
  extended_show_hide_alert()
  $(".owl-carousel").owlCarousel()
  
show_hide_alert = () ->
  $(".regular-alert").hide().fadeIn("slow").delay(5000).fadeOut("slow", () -> 
    $(this).remove())
    
extended_show_hide_alert = () ->
  $(".longer-alert").hide().fadeIn("slow").delay(5000000).fadeOut("slow", () -> 
    $(this).remove())

set_active = () -> 
  pathname = document.location.pathname
  root = '.nav.navbar-nav '
  first_path = root + ' >li > a[href="' + pathname + '"]'
  second_path = root + '>li.dropdown>ul.dropdown-menu>li>a[href="' + pathname + '"]'
  $(first_path).parents('li,ul').addClass('active');
  $(second_path).parents('li,ul').addClass('active');

set_swal_defaults = () ->
  swal.setDefaults({ confirmButtonColor: '#3C8DBC' });
  
