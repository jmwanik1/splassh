var markers = [];
var markerIds = {};
var openInfo;
var map
var tracker_map;
var markersTwo = [];
var markerIdsTwo = {};
var openInfoTwo;

function initMap(positions) {

  var infoWindow = new google.maps.InfoWindow({});
  
  map = new google.maps.Map(document.getElementById('map'), {
     center: {lat: 37.939, lng: -99.17057},
     zoom: 1,
  });
  
  var locations = [];
  var x = "";
  var y = "";
  for(var i in positions) {
    // set pincolor for the marke
    x = parseFloat(positions[i].lat);
    y = parseFloat(positions[i].lng);
    locations.push({
      lat: x,
      lng: y,
      title: positions[i].name,
      created_at: positions[i].created_at,
      updated_at: positions[i].updated_at,
      slug: positions[i].id

     })
  }
  
  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  for(var i in locations) {
    // add location to marker id, will help when calling on click 
    // to open info box outside map
    markerIds[positions[i].id] = i;
    // set pin color 
    var pinColor = "";
    if ($("#item_" + positions[i].id).length > 0)
      pinColor = $("#item_" + positions[i].id).data("color").replace("#","");
    else if($("#tag_" + positions[i].id).length > 0)
      pinColor = $("#tag_" + positions[i].id).data("color").replace("#","");
      
    // Create a marker 
    var marker = new google.maps.Marker({
      position: {lat: locations[i].lat, lng: locations[i].lng},
      map: map,
      label: locations[i].label,
      zIndex: Math.round(locations[i].lat * -100000) << 5
    });
    
    if(pinColor)
      marker.setIcon(mapPinImage(pinColor));
    
    // Add a listen to the map when it's clicked, open the info box
    google.maps.event.addListener(marker, 'click', (function(marker, i){
      return function() {
        infoWindow.setContent(
          "<p><b>Project Name: </b>" + "<a href=/projects/" + locations[i].slug + ">" + locations[i].title + "</p></a>" +

          // "<p><b>Item: </b>" + locations[i].title + "</p>" +
          "<p><b>Record Date: </b>" + locations[i].created_at + "</p>" +
          "<p><b>Update Date: </b>" + locations[i].updated_at + "</p>"
        );
      
        infoWindow.open(map, marker);
        map.setZoom(12);
        map.setCenter(marker.getPosition());
      }
    })(marker, i));
    
    // Add a listen to the marker to allow the infobox to be opened outside
    // the map
    markers.push(marker);
    
   google.maps.event.addListener(map, 'click', function() {
        infoWindow.close();
    });
  }
  
  // The function to trigger the marker click, 'id' is the reference index to the 'markers' array.
  openInfo = function (id){
      var markerLocation = markerIds[id];
      google.maps.event.trigger(markers[markerLocation], 'click');
  }
}


// Returns a map marker with the given color 
function mapPinImage(pinColor) {
  var url = "https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|";
  var pinImage = new google.maps.MarkerImage(url + pinColor,
      new google.maps.Size(21, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(10, 34));
  return pinImage;
}

function initTrackerMap(positionsTwo) {
  var infoWindowTwo = new google.maps.InfoWindow({});
  
  tracker_map = new google.maps.Map(document.getElementById('tracker_map'), {
    center: {lat: 37.939, lng: -99.17057},
    zoom: 1,
  });
  

  var locationsTwo = [];
  var x = "";
  var y = "";
  for(var i in positionsTwo) {
    // set pincolor for the marke
    x = parseFloat(positionsTwo[i].lat);
    y = parseFloat(positionsTwo[i].lng);
    locationsTwo.push({
      lat: x,
      lng: y,
      title: positionsTwo[i].name,
      created_at: positionsTwo[i].created_at,
      updated_at: positionsTwo[i].updated_at,
      slug: positionsTwo[i].id

    })
  }
  
  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  for(var i in locationsTwo) {
    // add location to marker id, will help when calling on click 
    // to open info box outside map
    markerIdsTwo[positionsTwo[i].id] = i;
    // set pin color 
    var pinColor = "";
    if ($("#item_" + positionsTwo[i].id).length > 0)
      pinColor = $("#item_" + positionsTwo[i].id).data("color").replace("#","");
    else if($("#tag_" + positionsTwo[i].id).length > 0)
      pinColor = $("#tag_" + positionsTwo[i].id).data("color").replace("#","");
      
    // Create a marker 
    var markerTwo = new google.maps.Marker({
      position: {lat: locationsTwo[i].lat, lng: locationsTwo[i].lng},
      map: tracker_map,
      label: locationsTwo[i].label,
      zIndex: Math.round(locationsTwo[i].lat * -100000) << 5
    });
    
    if(pinColor)
      markerTwo.setIcon(mapPinImage(pinColor));
    
    // Add a listen to the map when it's clicked, open the info box
    google.maps.event.addListener(markerTwo, 'click', (function(markerTwo, i){
      return function() {
        infoWindowTwo.setContent(
          // "<p><a href=\"locationsTwo[i].slug\"><b>Item: </b>" + locationsTwo[i].title + "</p></a>" +
          "<p><b>Tracker Name: </b>" + "<a href=/trackers/" + locationsTwo[i].slug + ">" + locationsTwo[i].title + "</p></a>" +

          "<p><b>Record Date: </b>" + locationsTwo[i].created_at + "</p>" +
          "<p><b>Update Date: </b>" + locationsTwo[i].updated_at + "</p>"
        );
        infoWindowTwo.open(tracker_map, markerTwo);
        tracker_map.setZoom(12);
        tracker_map.setCenter(markerTwo.getPosition());
      }
    })(markerTwo, i));
    
    // Add a listen to the marker to allow the infobox to be opened outside
    // the map
    markersTwo.push(markerTwo);
    
  google.maps.event.addListener(tracker_map, 'click', function() {
        infoWindowTwo.close();
    });
  }
  
  // The function to trigger the marker click, 'id' is the reference index to the 'markers' array.
  openInfoTwo = function (id){
      var markerLocation = markerIdsTwo[id];
      google.maps.event.trigger(markersTwo[markerLocation], 'click');
  }
}


function displayMap(trackers) {
    var tracker_positions = trackers
 
    document.getElementById('tracker_map').style.display="block";
    setTimeout(function () { initTrackerMap(tracker_positions); }, 300);      
}
