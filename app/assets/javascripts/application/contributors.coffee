$(document).on 'turbolinks:load', ->
  validateSearch()
  
$(document).ready(validateSearch)

validateSearch = () ->
  $("#search-form").validate 
    debug: false
    rules: 
      "user[email]":
        required: true
        email: true