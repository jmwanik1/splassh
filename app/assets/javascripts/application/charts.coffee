class Charts 
  constructor: () ->
  
  # ========================= Helper methods =====================
  series_data: (data, data_info, type="") ->
    series = []
    for obj of data
      series.push
        type: type
        name: data_info[obj].title
        data: data[obj]
    series
  
  series_data_average: (data, data_info) ->
    series = []
    for obj of data
      series.push
        name: data_info[obj].title
        y: this.average(data[obj])
        drilldown: data_info[obj].title
    series
  
  series_data_percent: (data, data_info) ->
    series = []
    total = 0
    # add up the total averages 
    for obj of data
      avg = this.average(data[obj])
      total += avg
      
    # divide and store the share for each dataset
    for obj of data
      series.push
        name: data_info[obj].title
        # start data, average 
        y: (avg / this.average(data[obj])) * 100
        drilldown: data_info[obj].title
    series
    
  average: (data) ->
    sum = 0
    i = 0
    while i < data.length
      if data[i][1] >= 0 && data[i][1] < 15
        sum += data[i][1]
      i++
    sum /= data.length
    sum
  
  # ========================= End Helper Methods ==================
  
  #==========================     Charts      =====================
  spline: (data, data_info) -> 
    Highcharts.chart 'chart-container',
      chart: type: 'spline'
      title: text: "Line Chart of pH data"
      subtitle: text: 'Source: http://erddap.secoora.org'
      xAxis:
        type: 'datetime'
        dateTimeLabelFormats:
          month: '%e. %b'
          year: '%b'
        title: text: 'Date'
      yAxis:
        title: text: 'pH'
        floor: 0
        ceiling: 15
      credits: enabled: false
      exporting: enabled: true
      tooltip:
        headerFormat: '<b>{series.name}</b><br>'
        pointFormat: '{point.x:%e %b %y} <b>pH: {point.y:.2f}</b>'
      plotOptions: spline: marker: enabled: true
      series: this.series_data(data, data_info)
    
  spline_area: (data, data_info) ->
    Highcharts.chart 'chart-container',
      chart: type: 'areaspline'
      title: text: "Area Chart of pH data"
      subtitle: text: 'Source: http://erddap.secoora.org'
      legend: enabled: true
      xAxis:
        type: 'datetime'
        dateTimeLabelFormats:
          month: '%e. %b'
          year: '%b'
        title: text: 'Date'
      yAxis: 
        title: 
          text: 'pH'
        floor: 0
        ceiling: 15
      tooltip:
        shared: true
        valueSuffix: ' pH'
      credits: enabled: false
      exporting: buttons: contextButton: enabled: true
      plotOptions: areaspline: fillOpacity: 0.5
      series: this.series_data(data, data_info)
  
  area_zoomable: (data, data_info) ->
    Highcharts.chart 'chart-container',
      chart: zoomType: 'x'
      title: text: "Zoomable Area Chart of pH data"
      subtitle: text: if document.ontouchstart == undefined then 'Click and drag in the plot area to zoom in' else 'Pinch the chart to zoom in'
      subtitle: text: 'Source: http://erddap.secoora.org'
      xAxis: type: 'datetime'
      yAxis: 
        title: text: 'pH'
        floor: 0
        ceiling: 15
      legend: enabled: true
      exporting: buttons: contextButton: enabled: true
      credits: enabled: false
      plotOptions: area:
        fillOpacity: 0.5
        lineWidth: 1
        states: hover: lineWidth: 1
        threshold: null
      series: this.series_data(data, data_info, "area")
  
  bar_and_column: (data, data_info, type) ->
    text = ''
    if type.toLowerCase() is 'bar'
      text = 'Bar'
    else
      text = 'Column'
    Highcharts.chart 'chart-container',
      chart: type: type
      title: text: "#{text} Chart of Average pH"
      subtitle: text: 'Source: <a href="http://erddap.secoora.org">erddap.secoora.com</a>.'
      xAxis: type: 'category'
      yAxis: 
        title: text: 'pH'
        floor: 0
        ceiling: 15
      legend: enabled: true
      credits: enabled: false
      plotOptions: series:
        borderWidth: 0
        dataLabels:
          enabled: true
          format: '{point.y:.1f} pH'
      tooltip:
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>'
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} pH</b><br/>'
      series: [{
        name: 'pH'
        colorByPoint: true
        data: this.series_data_average(data, data_info)
      }]
  
  pie: (data, data_info) ->
    Highcharts.chart 'chart-container',
      chart:
        plotBackgroundColor: null
        plotBorderWidth: null
        plotShadow: false
        type: 'pie'
      title: text: 'Pie Chart of pH Between Datasets'
      subtitle: text: 'Source: <a href="http://erddap.secoora.org">erddap.secoora.com</a>.'
      tooltip: pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      credits: enabled: false
      plotOptions: pie:
        allowPointSelect: true
        cursor: 'pointer'
        dataLabels:
          enabled: true
          format: '<b>{point.name}</b>: {point.percentage:.1f} % of total pH'
          style: color: Highcharts.theme and Highcharts.theme.contrastTextColor or 'black'
      series: [{
        name: 'pH Percentage'
        colorByPoint: true
        data: this.series_data_percent(data, data_info)
      }]
      
  scatter_plot: (data, data_info) ->
    # add the regression data
    series = this.series_data(data, data_info)
    object_data_length = series[series.length-1].data.length
    series.push
      type: 'line'
      name: 'Regression Line'
      data: [series[0].data[0], 
             series[series.length-1].data[object_data_length-1]
            ]
      marker: enabled: false
      states: hover: lineWidth: 0
      enableMouseTracking: false
      
    Highcharts.chart 'chart-container',
      chart:
        type: 'scatter'
        zoomType: 'xy'
      title: text: 'Scatter plot of pH Data'
      subtitle: text: 'Source: <a href="http://erddap.secoora.org">erddap.secoora.com</a>.'
      xAxis:
        type: 'datetime'
        title:
          enabled: true
          text: 'Date and Time'
        startOnTick: true
        endOnTick: true
        showLastLabel: true
      yAxis: 
        title: text: 'pH'
        floor: 0
        ceiling: 15
      legend: enabled: true
      credits: enabled: false
      plotOptions: scatter:
        states: hover: marker: enabled: false
        tooltip:
          headerFormat: '<b>{series.name}</b><br>'
          pointFormat: '{point.x:%e %b %y} , {point.y} pH'
      series: series


# Make the charts available on the global scope
window.Charts = Charts