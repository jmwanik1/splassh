App.notifications = App.cable.subscriptions.create "NotificationsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    $("#item-link").append(
                  "<span class= 'label label-danger' id= 'unread-count'>
                  #{data.unread}</span>")
    $("#notifications-item").append("<a class='dropdown-item' href='#{data.url}'>
                                      #{data.action} 
                                      </a>")
  
$(document).on 'turbolinks:load', ->
  clear_notifications()

clear_notifications = () ->
  $("#notifications-link").click ->
    handleClick()

handleClick = () ->
  $.ajax(
    url: "/notifications/mark_as_read",
    method: "POST",
    dataType: "JSON",
    success: handleSuccess()
  )

handleSuccess = () ->
  $("#unread-count").text("0")
  $("#notifications-header").text("You have no new notifications") 