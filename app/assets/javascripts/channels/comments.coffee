App.comments = App.cable.subscriptions.create "CommentsChannel",
  
  # collection of comments on the page
  collection: -> $("#discussions")

  # Called when the subscription is ready for use on the server
  connected: ->
    setTimeout =>
      @followCurrentProject()
    , 1000
  # Called when the subscription has been terminated by the server
  disconnected: ->

  # Called when there's incoming data on the websocket for this channel
  received: (data) ->
    # Show comment on page
    element_id = "#comments_#{data.commentable_id}"
    console.log(@userIsCurrentUser(data.comment))
    $(element_id).prepend(data.comment) unless @userIsCurrentUser(data.comment)
    
  followCurrentProject: ->
    # Called to ensure the user is on a project page to broadcast comments
    projectId = @collection().data('project-id')
    if projectId
      @perform 'follow', project_id: projectId
    else
      @perform 'unfollow'
  
  userIsCurrentUser: (comment) ->
    console.log($(comment).attr('data-user-id'))
    console.log($('meta[name=current-user]').attr('id'))
    $(comment).attr('data-user-id') is $('meta[name=current-user]').attr('id')
    
$(document).on 'turbolinks:load', ->
  # Do something after page load
  submitComment()
  
submitComment = () ->
  # Clear the comment text box when the submit button is clicked
  form_submit_btn = ""
  $(".comment_content").on 'click', ->
    form_submit_btn = getSubmitButton($(this))
    console.log(form_submit_btn)
    
  $(".comment_content").on 'keydown', ->
    if event.keyCode is 13
      $("##{form_submit_btn}").click()
      event.target.value = ""
      event.preventDefault()
      $("#no-comment").remove()
    
getSubmitButton = (element) ->
  # strip away the text and get the textbox id which matches the correspodning
  # submit button
  id = element.attr('id').replace( /[^\d.]/g, '')
  form_submit_btn = "comment_submit_#{id}"
  return form_submit_btn
  