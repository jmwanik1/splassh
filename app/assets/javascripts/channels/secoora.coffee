App.secoora = App.cable.subscriptions.create "SecooraChannel",
  
  collection: -> 
  
  connected: ->
    
  disconnected: ->

  received: (data) ->
    @appendLine(data)
 
  appendLine: (data) ->
    html = @createLine(data)
    $("#progress").empty().append(html)
    console.log("hd")
  createLine: (data) ->
    current = data.current
    total = data.total
    percent = current/total * 100
    if percent != 100
      """
        <div class="progress active">
          <div class="progress-bar progress-bar-primary progress-bar-striped" 
            role="progressbar" aria-valuenow="#{percent}" 
            aria-valuemin="0" aria-valuemax="100" style="width:#{percent}%">
            <p>#{Math.round(percent)}%</p>
            <span class="sr-only">#{percent}% Complete</span>
          </div>
        </div>
      """
    else if percent == 100
      """
      <p> Finished </p>
      """