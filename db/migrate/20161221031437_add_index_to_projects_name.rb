class AddIndexToProjectsName < ActiveRecord::Migration[5.0]
  def change
    add_index :projects, :name
  end
end
