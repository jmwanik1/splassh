class AddProjectsToSites < ActiveRecord::Migration[5.0]
  def change
    add_reference :sites, :project, index: true
    add_foreign_key :sites, :projects, column: :project_id 
  end
end
