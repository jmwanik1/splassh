class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :name, null: false 
      t.text :about, null: false
      t.string :role,  null: false
      t.string :image
      t.string :website
      t.string :facebook
      t.string :linkedin
      t.string :twitter

      t.timestamps
    end
  end
end
