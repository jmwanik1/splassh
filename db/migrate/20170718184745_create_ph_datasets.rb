class CreatePhDatasets < ActiveRecord::Migration[5.0]
  def change
    create_table :ph_datasets do |t|
      t.string :dataset_id,           null: false, index: true
      t.string :title,                null: false
      t.string :institution,          null: false
      t.string :url,                  null: false
      t.string :cdm_timeseries_variables
      t.string :time_coverage_start
      t.string :time_coverage_end
      t.decimal :geospatial_lat_max, {:precision=>10, :scale=>6}
      t.decimal :geospatial_lon_max, {:precision=>10, :scale=>6}
      t.json    :dataset
      t.timestamps
    end
  end
end
