class CreateScientificData < ActiveRecord::Migration[5.0]
  def change
    create_table :scientific_data do |t|
      t.decimal :value
      t.string :unit
      t.string :instrument
      t.string :technique
      t.string :calibration
      t.text :info
      
      t.timestamps
    end
  end
end
