class CreateFieldNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :field_notes do |t|
      t.string :note
      
      t.timestamps
      t.references :user, foreign_key: true
      t.references :project, foreign_key: true
    end
  end
end
