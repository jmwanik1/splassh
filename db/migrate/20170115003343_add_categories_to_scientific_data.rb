class AddCategoriesToScientificData < ActiveRecord::Migration[5.0]
  def change
    add_reference :scientific_data, :category, index: true
    add_foreign_key :scientific_data, :categories, column: :category_id
  end
end
