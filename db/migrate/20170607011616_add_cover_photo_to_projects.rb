class AddCoverPhotoToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :cover_photo, :string
  end
end
