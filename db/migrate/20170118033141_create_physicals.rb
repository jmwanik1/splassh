class CreatePhysicals < ActiveRecord::Migration[5.0]
  def change
    create_table :physicals do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
