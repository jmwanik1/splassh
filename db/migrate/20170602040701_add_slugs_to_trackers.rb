class AddSlugsToTrackers < ActiveRecord::Migration[5.0]
  def change
    add_column :trackers, :slug, :string
    add_index :trackers, :slug
  end
end
