class AddAvailableToStates < ActiveRecord::Migration[5.0]
  def change
    add_column :states, :available, :boolean, default: true
  end
end
