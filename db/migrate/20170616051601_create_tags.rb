class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.string :name
      t.string :color
      t.text :description

      t.timestamps
    end
    add_reference :projects, :tag, index: true, foreign_key: true
  end
end
