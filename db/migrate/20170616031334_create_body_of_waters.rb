class CreateBodyOfWaters < ActiveRecord::Migration[5.0]
  def change
    create_table :body_of_waters do |t|
      t.string :name
      t.text :desc
      t.string :color

      t.timestamps
    end
    
    add_reference :projects, :body_of_water, index: true, foreign_key: true
  end
end
