class AddBiologicalToScientificData < ActiveRecord::Migration[5.0]
  def change
    add_reference :scientific_data, :biological, index: true
    add_foreign_key :scientific_data, :biologicals, column: :biological_id
  end
end
