class AddProjectToTrackers < ActiveRecord::Migration[5.0]
  def change
    add_reference :trackers, :project, foreign_key: true
  end
end
