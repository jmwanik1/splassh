class CreateStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :statuses do |t|
      t.text :content
      t.references :item, foreign_key: true
      t.references :author

      t.timestamps null: false
    end
    
    add_foreign_key :statuses, :users, column: :author_id
  end
end
