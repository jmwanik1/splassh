class AddPreviousStateToStatuses < ActiveRecord::Migration[5.0]
  def change
    add_reference :statuses, :previous_state, index: true
    add_foreign_key :statuses, :states, column: :previous_state_id
  end
end
