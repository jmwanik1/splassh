class AddProcessedToSecooraDatasets < ActiveRecord::Migration[5.0]
  def change
    add_column :secoora_datasets, :processed, :boolean, default: false 
  end
end
