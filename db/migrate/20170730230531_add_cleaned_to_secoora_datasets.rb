class AddCleanedToSecooraDatasets < ActiveRecord::Migration[5.0]
  def change
    add_column :secoora_datasets, :cleaned, :boolean, default: false 
  end
end
