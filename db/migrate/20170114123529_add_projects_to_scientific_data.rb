class AddProjectsToScientificData < ActiveRecord::Migration[5.0]
  def change
    add_reference :scientific_data, :project, index: true
    add_foreign_key :scientific_data, :projects, column: :project_id 
  end
end
