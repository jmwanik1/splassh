class AddFieldsToSites < ActiveRecord::Migration[5.0]
  def change
    
      add_column :sites, :lat, :decimal, {:precision=>10, :scale=>6}
      add_column :sites, :lng, :decimal, {:precision=>10, :scale=>6}
      add_column :sites, :name, :string
      add_column :sites, :description, :text
      add_reference :sites, :user, foreign_key: true

    end
end
