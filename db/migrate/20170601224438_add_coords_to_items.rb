class AddCoordsToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :lat, :decimal, {:precision=>10, :scale=>6}
    add_column :items, :lng, :decimal, {:precision=>10, :scale=>6}
  end
end
