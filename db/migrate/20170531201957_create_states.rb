class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string :name
      t.string :color

      t.timestamps
    end
    
    add_reference :items, :state, index: true, foreign_key: true
    add_reference :statuses, :state, foreign_key: true
  end
end
