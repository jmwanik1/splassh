class AddCoordsToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :lat, :decimal, {:precision=>10, :scale=>6}
    add_column :projects, :lng, :decimal, {:precision=>10, :scale=>6}
  end
end
