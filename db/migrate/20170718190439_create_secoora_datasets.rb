class CreateSecooraDatasets < ActiveRecord::Migration[5.0]
  def change
    create_table :secoora_datasets do |t|
      t.string :dataset_url
      t.string :about

      t.timestamps
    end
  end
end
