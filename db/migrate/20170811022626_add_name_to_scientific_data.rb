class AddNameToScientificData < ActiveRecord::Migration[5.0]
  def change
    add_column :scientific_data, :name, :string
  end
end
