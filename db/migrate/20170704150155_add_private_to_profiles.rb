class AddPrivateToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :private, :boolean, default: false
  end
end
