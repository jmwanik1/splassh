class CreateDebris < ActiveRecord::Migration[5.0]
  def change
    create_table :debris do |t|
      t.string :title, null: false
      t.string :status, null: false
      t.text :description, null: false
      t.decimal :lat, precision: 10, scale: 6, null: false
      t.decimal :lng, precision: 10, scale: 6, null: false
      
      t.timestamps null: false
    end
  end
end
