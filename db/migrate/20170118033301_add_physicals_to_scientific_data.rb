class AddPhysicalsToScientificData < ActiveRecord::Migration[5.0]
  def change
    add_reference :scientific_data, :physical, index: true
    add_foreign_key :scientific_data, :physicals, column: :physical_id
  end
end
