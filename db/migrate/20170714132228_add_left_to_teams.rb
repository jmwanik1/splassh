class AddLeftToTeams < ActiveRecord::Migration[5.0]
  def change
    add_column :teams, :left, :boolean, default: false
  end
end
