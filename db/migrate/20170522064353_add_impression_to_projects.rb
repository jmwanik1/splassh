class AddImpressionToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :impressions_count, :integer
  end
end
