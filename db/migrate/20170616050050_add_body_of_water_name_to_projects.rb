class AddBodyOfWaterNameToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :body_of_water_name, :string
  end
end
