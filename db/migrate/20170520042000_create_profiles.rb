class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :firstname, null: false
      t.string :lastname, null: false
      t.text :about
      t.string :school
      t.string :educationlevel
      t.string :tag 
      t.references :user, foreign_key: true
    end
  end
end
