class PreventNullValuesInSites < ActiveRecord::Migration[5.0]
  def change
    change_column_null :sites, :lat, false
    change_column_null :sites, :lng, false
    change_column_null :sites, :name, false
    change_column_null :sites, :description, false

  end
end