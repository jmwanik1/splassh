class AddPrivateToTrackers < ActiveRecord::Migration[5.0]
  def change
    add_column :trackers, :private, :boolean, default: false
  end
end
