class AddChemicalsToScientificData < ActiveRecord::Migration[5.0]
  def change
    add_reference :scientific_data, :chemical, index: true
    add_foreign_key :scientific_data, :chemicals, column: :chemical_id
  end
end
