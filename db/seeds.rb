# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
unless User.exists?(email: "admin@splassh.org") 
  user = User.create!(email: "admin@splassh.org", 
               password: ENV["SPLASSH_ADMIN_PASSWORD"], admin: true)
  Profile.create!(firstname: "Splassh", lastname: "Admin", user: user)
  puts "Admin created"
end

["Chemical", "Biological", "Physical"].each do |cat|
  unless Category.exists?(name: cat)
    Category.create!(name: cat)
  end  
end

["Nitrogen", "Nitrites", "Nitrates", "Ammonia", "Phosphorous", 
"Phosphates", "Alkalinity", "Carbon Dioxide", "Dissolved Oxygen",
"Biological Oxygen Demand", "Salinity", 
"Chlorophyll A", "pH", "Conductivity"].each do |chem|
  unless Chemical.exists?(name: chem)
    Chemical.create!(name: chem)
  end
end

["Fecal coliform Bacteria", "Non-fecal coliform bacteria", "Phytoplankton",
"Zooplankton", "Algae", "Blue Green Algae", "Plants", "Invertebrates", "Fish",
"Amphibians", "Reptiles", "Birds", "Mammals"].each do |organism|
  unless Biological.exists?(name: organism)
    Biological.create!(name: organism)
  end
end

["Clarity", "Flow", "Turbidity", "Conductivity","Depth", 
"Area"].each do |attribute|
  unless Physical.exists?(name: attribute)
    Physical.create!(name: attribute)
  end
end

State.create(name: "Detected", color: "#00c0ef", default: true)
State.create(name: "Unresolved", color: "#f39c12")
State.create(name: "Resolved", color: "#00a65a")
State.create(name: "Urgent", color: "#dd4b39")

Tracker.create(name: "Debris Tracker", description: "Tracks debris all over the world!", 
  lat: 34.1, lng: -19.1, user: User.first)

BodyOfWater.create(name: "Rivers")
BodyOfWater.create(name: "Lakes")
BodyOfWater.create(name: "Reservoirs")
BodyOfWater.create(name: "Streams")
BodyOfWater.create(name: "Wetlands (freshwater)")
BodyOfWater.create(name: "Tidal Creeks")
BodyOfWater.create(name: "Estuaries")
BodyOfWater.create(name: "Marshes (coastal)")
BodyOfWater.create(name: "Beaches")
BodyOfWater.create(name: "Reef")
BodyOfWater.create(name: "Coastal Ocean")
BodyOfWater.create(name: "Open Ocean")

Tag.create(name: "Pollution & Public Health", color: "#797B87")
Tag.create(name: "Fish Kills, Algae Blooms, & Dead Zones", color: "#797B87")
Tag.create(name: "Fisheries & Aquaculture", color: "#797B87")
Tag.create(name: "Recreation", color: "#797B87")
Tag.create(name: "Aquatic Life & Ecosystem", color: "#797B87")
Tag.create(name: "Technology & Innovation", color: "#797B87")
Tag.create(name: "Development & Habitat Destruction", color: "#797B87")
Tag.create(name: "Rerouted Waterways", color: "#797B87")
Tag.create(name: "Storms, Devastation, & Climate Change", color: "#797B87")
Tag.create(name: "Water Monitoring", color: "#797B87")
