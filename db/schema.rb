# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180103142350) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "trackable_type"
    t.integer  "trackable_id"
    t.string   "action"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["user_id"], name: "index_activities_on_user_id", using: :btree
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "file"
    t.integer  "item_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_attachments_on_item_id", using: :btree
    t.index ["project_id"], name: "index_attachments_on_project_id", using: :btree
  end

  create_table "biologicals", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "body_of_waters", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.string   "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "chemicals", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "ancestry"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.string   "old_author"
    t.string   "old_project_id"
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "contributors", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "contributable_type"
    t.integer  "contributable_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["contributable_type", "contributable_id"], name: "index_contributors_on_contributable_type_and_contributable_id", using: :btree
    t.index ["user_id"], name: "index_contributors_on_user_id", using: :btree
  end

  create_table "debris", force: :cascade do |t|
    t.string   "title",                                null: false
    t.string   "status",                               null: false
    t.text     "description",                          null: false
    t.decimal  "lat",         precision: 10, scale: 6, null: false
    t.decimal  "lng",         precision: 10, scale: 6, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "favorited_type"
    t.integer  "favorited_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["favorited_type", "favorited_id"], name: "index_favorites_on_favorited_type_and_favorited_id", using: :btree
    t.index ["user_id"], name: "index_favorites_on_user_id", using: :btree
  end

  create_table "field_notes", force: :cascade do |t|
    t.string   "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "project_id"
    t.index ["project_id"], name: "index_field_notes_on_project_id", using: :btree
    t.index ["user_id"], name: "index_field_notes_on_user_id", using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "impressions", force: :cascade do |t|
    t.string  "impressionable_type"
    t.integer "impressionable_id"
    t.integer "user_id"
    t.string  "ip_address"
  end

  create_table "items", force: :cascade do |t|
    t.string   "name",                                 null: false
    t.text     "description"
    t.integer  "tracker_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "state_id"
    t.decimal  "lat",         precision: 10, scale: 6
    t.decimal  "lng",         precision: 10, scale: 6
    t.integer  "user_id"
    t.index ["state_id"], name: "index_items_on_state_id", using: :btree
    t.index ["tracker_id"], name: "index_items_on_tracker_id", using: :btree
    t.index ["user_id"], name: "index_items_on_user_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "by_id"
    t.integer  "to_id"
    t.datetime "read_at"
    t.string   "action"
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "ph_datasets", force: :cascade do |t|
    t.string   "dataset_id",                                        null: false
    t.string   "title",                                             null: false
    t.string   "institution",                                       null: false
    t.string   "url",                                               null: false
    t.string   "cdm_timeseries_variables"
    t.string   "time_coverage_start"
    t.string   "time_coverage_end"
    t.decimal  "geospatial_lat_max",       precision: 10, scale: 6
    t.decimal  "geospatial_lon_max",       precision: 10, scale: 6
    t.json     "dataset"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.index ["dataset_id"], name: "index_ph_datasets_on_dataset_id", using: :btree
  end

  create_table "physicals", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string  "firstname",                      null: false
    t.string  "lastname",                       null: false
    t.text    "about"
    t.string  "school"
    t.string  "educationlevel"
    t.string  "tag"
    t.integer "user_id"
    t.boolean "private",        default: false
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name",                                        null: false
    t.text     "description",                                 null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.decimal  "lat",                precision: 10, scale: 6
    t.decimal  "lng",                precision: 10, scale: 6
    t.integer  "author_id"
    t.string   "slug"
    t.integer  "impressions_count"
    t.string   "cover_photo"
    t.integer  "body_of_water_id"
    t.string   "body_of_water_name"
    t.integer  "tag_id"
    t.string   "old_id"
    t.string   "owner_email"
    t.string   "old_author_id"
    t.index ["author_id"], name: "index_projects_on_author_id", using: :btree
    t.index ["body_of_water_id"], name: "index_projects_on_body_of_water_id", using: :btree
    t.index ["name"], name: "index_projects_on_name", using: :btree
    t.index ["slug"], name: "index_projects_on_slug", using: :btree
    t.index ["tag_id"], name: "index_projects_on_tag_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "role"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_roles_on_project_id", using: :btree
    t.index ["user_id"], name: "index_roles_on_user_id", using: :btree
  end

  create_table "scientific_data", force: :cascade do |t|
    t.decimal  "value"
    t.string   "unit"
    t.string   "instrument"
    t.string   "technique"
    t.string   "calibration"
    t.text     "info"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "project_id"
    t.integer  "category_id"
    t.integer  "chemical_id"
    t.integer  "physical_id"
    t.integer  "biological_id"
    t.integer  "user_id"
    t.string   "name"
    t.index ["biological_id"], name: "index_scientific_data_on_biological_id", using: :btree
    t.index ["category_id"], name: "index_scientific_data_on_category_id", using: :btree
    t.index ["chemical_id"], name: "index_scientific_data_on_chemical_id", using: :btree
    t.index ["physical_id"], name: "index_scientific_data_on_physical_id", using: :btree
    t.index ["project_id"], name: "index_scientific_data_on_project_id", using: :btree
    t.index ["user_id"], name: "index_scientific_data_on_user_id", using: :btree
  end

  create_table "secoora_datasets", force: :cascade do |t|
    t.string   "dataset_url"
    t.string   "about"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "cleaned",     default: false
    t.boolean  "processed",   default: false
  end

  create_table "sites", force: :cascade do |t|
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "project_id"
    t.decimal  "lat",         precision: 10, scale: 6, null: false
    t.decimal  "lng",         precision: 10, scale: 6, null: false
    t.string   "name",                                 null: false
    t.text     "description",                          null: false
    t.integer  "user_id"
    t.index ["project_id"], name: "index_sites_on_project_id", using: :btree
    t.index ["user_id"], name: "index_sites_on_user_id", using: :btree
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "color"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "default",    default: false
    t.boolean  "available",  default: true
  end

  create_table "statuses", force: :cascade do |t|
    t.text     "content"
    t.integer  "item_id"
    t.integer  "author_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "state_id"
    t.integer  "previous_state_id"
    t.index ["author_id"], name: "index_statuses_on_author_id", using: :btree
    t.index ["item_id"], name: "index_statuses_on_item_id", using: :btree
    t.index ["previous_state_id"], name: "index_statuses_on_previous_state_id", using: :btree
    t.index ["state_id"], name: "index_statuses_on_state_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.string   "color"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name",                       null: false
    t.text     "about",                      null: false
    t.string   "role",                       null: false
    t.string   "image"
    t.string   "website"
    t.string   "facebook"
    t.string   "linkedin"
    t.string   "twitter"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "left",       default: false
  end

  create_table "temp", id: false, force: :cascade do |t|
    t.text "values"
  end

  create_table "trackers", force: :cascade do |t|
    t.string   "name",                                                 null: false
    t.text     "description",                                          null: false
    t.decimal  "lat",         precision: 10, scale: 6
    t.decimal  "lng",         precision: 10, scale: 6
    t.integer  "user_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "slug"
    t.boolean  "private",                              default: false
    t.integer  "project_id"
    t.index ["project_id"], name: "index_trackers_on_project_id", using: :btree
    t.index ["slug"], name: "index_trackers_on_slug", using: :btree
    t.index ["user_id"], name: "index_trackers_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "name",                   default: "",    null: false
    t.datetime "archived_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "old_id"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "about"
    t.string   "education_level"
    t.string   "school"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "activities", "users"
  add_foreign_key "attachments", "items"
  add_foreign_key "attachments", "projects"
  add_foreign_key "comments", "users"
  add_foreign_key "contributors", "users"
  add_foreign_key "favorites", "users"
  add_foreign_key "field_notes", "projects"
  add_foreign_key "field_notes", "users"
  add_foreign_key "items", "states"
  add_foreign_key "items", "trackers"
  add_foreign_key "items", "users"
  add_foreign_key "profiles", "users"
  add_foreign_key "projects", "body_of_waters"
  add_foreign_key "projects", "tags"
  add_foreign_key "projects", "users", column: "author_id"
  add_foreign_key "roles", "projects"
  add_foreign_key "roles", "users"
  add_foreign_key "scientific_data", "biologicals"
  add_foreign_key "scientific_data", "categories"
  add_foreign_key "scientific_data", "chemicals"
  add_foreign_key "scientific_data", "physicals"
  add_foreign_key "scientific_data", "projects"
  add_foreign_key "scientific_data", "users"
  add_foreign_key "sites", "projects"
  add_foreign_key "sites", "users"
  add_foreign_key "statuses", "items"
  add_foreign_key "statuses", "states"
  add_foreign_key "statuses", "states", column: "previous_state_id"
  add_foreign_key "statuses", "users", column: "author_id"
  add_foreign_key "trackers", "projects"
  add_foreign_key "trackers", "users"
end
